if [[ "$#" != "1" || ("$1" != "MG" && "$1" != "all") ]] ; then 
  echo "You need to speficy whether to setup MG only or everything"
  echo "source setup.sh MG/all"
  return 
fi

if [[ "$0" == *"bash"* ]] ; then
  export BASEDIR=$(echo $BASH_SOURCE | xargs realpath | xargs dirname )
else 
  export BASEDIR=$(echo $0 | xargs realpath | xargs dirname )
fi
echo "Set BASEDIR=$BASEDIR"

# Set up LHAPDF
export PATH=$PATH:$BASEDIR/LHAPDF-6.3.0/gcc620_x86_64-slc6/bin

if [[ "$1" == "MG" ]] ; then return ; fi

# Set up gcc
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase  
. ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
GCCVER="gcc620_x86_64_slc6"
lsetup "gcc $GCCVER"

# Set up rivet
source $BASEDIR/Rivet-3.1.0/rivetenv.sh

# Set up yoda
source $BASEDIR/YODA-1.8.0/yodaenv.sh

# Set up root
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"

# Set up pythia 
PYTHIAINSTALLDIR=$(echo $BASEDIR/pythia8303/$GCCVER | sed 's|_slc|-slc|')
if [ -d "$PYTHIAINSTALLDIR" ] ; then
  echo "Setting up: $PYTHIAINSTALLDIR"
  export LD_LIBRARY_PATH=$PYTHIAINSTALLDIR/lib:$LD_LIBRARY_PATH
else 
  echo "Pythia installation: $PYTHIAINSTALLDIR not found"
fi 
