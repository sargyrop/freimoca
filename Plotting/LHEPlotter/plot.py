#!/usr/bin/env python3

from LHEParser import *

def fillFromLHE(lhe, h_theta, h_pt):
    for event in lhe:
        Wp = FourMomentum()
        for part in event:
            momenta = FourMomentum(part)
            if part.pid == 24:
                Wp += momenta

        h_theta.Fill(Wp.theta)
        h_pt.Fill(Wp.pt)

if '__main__' == __name__:   
    
    lhe_d = EventFile('ttbar/Events/run_01/unweighted_events_decayed_density.lhe')
    lhe_f = EventFile('ttbar/Events/run_01/unweighted_events_decayed_full.lhe')
    lhe_o = EventFile('ttbar/Events/run_01/unweighted_events_decayed_onshell.lhe')
    lhe_n = EventFile('ttbar/Events/run_01/unweighted_events_decayed_none.lhe')
    
    # Histograms
    import ROOT
    h_theta_W_d = ROOT.TH1F("h_theta_W_d","h_theta_W_d",32,0,1.6)
    h_pt_W_d = ROOT.TH1F("h_pt_W_d","h_pt_W_d",100,0,500)
    h_theta_W_f = ROOT.TH1F("h_theta_W_f","h_theta_W_f",32,0,1.6)
    h_pt_W_f = ROOT.TH1F("h_pt_W_f","h_pt_W_f",100,0,500)
    h_theta_W_n = ROOT.TH1F("h_theta_W_n","h_theta_W_n",32,0,1.6)
    h_pt_W_n = ROOT.TH1F("h_pt_W_n","h_pt_W_n",100,0,500)
    c = ROOT.TCanvas()
    
    fillFromLHE(lhe_d, h_theta_W_d, h_pt_W_d)
    fillFromLHE(lhe_o, h_theta_W_f, h_pt_W_f)
    fillFromLHE(lhe_n, h_theta_W_n, h_pt_W_n)

    h_theta_W_d.Draw();
    h_theta_W_f.SetMarkerStyle(24);
    h_theta_W_f.SetMarkerColor(ROOT.EColor.kRed);
    h_theta_W_f.SetLineColor(ROOT.EColor.kRed);
    h_theta_W_n.SetMarkerStyle(25);
    h_theta_W_n.SetMarkerColor(ROOT.EColor.kGreen+1);
    h_theta_W_n.SetLineColor(ROOT.EColor.kGreen+1);
    h_theta_W_f.Draw("PE1 same");
    h_theta_W_n.Draw("PE1 same");
    c.Print("theta_W.png")

    h_pt_W_d.Draw();
    h_pt_W_f.SetMarkerStyle(24);
    h_pt_W_f.SetMarkerColor(ROOT.EColor.kRed);
    h_pt_W_f.SetLineColor(ROOT.EColor.kRed);
    h_pt_W_n.SetMarkerStyle(25);
    h_pt_W_n.SetMarkerColor(ROOT.EColor.kGreen+1);
    h_pt_W_n.SetLineColor(ROOT.EColor.kGreen+1);
    h_pt_W_f.Draw("PE1 same");
    h_pt_W_n.Draw("PE1 same");
    c.Print("pt_W.png")
    
if '__main__' == __name__:   
    import sys
    import os 
    sys.path.append('../../')
    root = os.path.dirname(__file__)
    if os.path.basename(root) == 'internal':
            __package__ = "internal"
            sys.path.append(os.path.dirname(root))
            import internal
    else:
        __package__ = "madgraph.various"

    lhe_d = EventFile('ttbar/Events/run_01/unweighted_events_decayed_density.lhe')
    lhe_f = EventFile('ttbar/Events/run_01/unweighted_events_decayed_full.lhe')
    lhe_o = EventFile('ttbar/Events/run_01/unweighted_events_decayed_onshell.lhe')
    lhe_n = EventFile('ttbar/Events/run_01/unweighted_events_decayed_none.lhe')
    
    # Histograms
    import ROOT
    h_theta_W_d = ROOT.TH1F("h_theta_W_d","h_theta_W_d",32,0,1.6)
    h_pt_W_d = ROOT.TH1F("h_pt_W_d","h_pt_W_d",100,0,500)
    h_theta_W_f = ROOT.TH1F("h_theta_W_f","h_theta_W_f",32,0,1.6)
    h_pt_W_f = ROOT.TH1F("h_pt_W_f","h_pt_W_f",100,0,500)
    h_theta_W_n = ROOT.TH1F("h_theta_W_n","h_theta_W_n",32,0,1.6)
    h_pt_W_n = ROOT.TH1F("h_pt_W_n","h_pt_W_n",100,0,500)
    c = ROOT.TCanvas()
    
    fillFromLHE(lhe_d, h_theta_W_d, h_pt_W_d)
    fillFromLHE(lhe_o, h_theta_W_f, h_pt_W_f)
    fillFromLHE(lhe_n, h_theta_W_n, h_pt_W_n)

    h_theta_W_d.Draw();
    h_theta_W_f.SetMarkerStyle(24);
    h_theta_W_f.SetMarkerColor(ROOT.EColor.kRed);
    h_theta_W_f.SetLineColor(ROOT.EColor.kRed);
    h_theta_W_n.SetMarkerStyle(25);
    h_theta_W_n.SetMarkerColor(ROOT.EColor.kGreen+1);
    h_theta_W_n.SetLineColor(ROOT.EColor.kGreen+1);
    h_theta_W_f.Draw("PE1 same");
    h_theta_W_n.Draw("PE1 same");
    c.Print("theta_W.png")

    h_pt_W_d.Draw();
    h_pt_W_f.SetMarkerStyle(24);
    h_pt_W_f.SetMarkerColor(ROOT.EColor.kRed);
    h_pt_W_f.SetLineColor(ROOT.EColor.kRed);
    h_pt_W_n.SetMarkerStyle(25);
    h_pt_W_n.SetMarkerColor(ROOT.EColor.kGreen+1);
    h_pt_W_n.SetLineColor(ROOT.EColor.kGreen+1);
    h_pt_W_f.Draw("PE1 same");
    h_pt_W_n.Draw("PE1 same");
    c.Print("pt_W.png")
                        