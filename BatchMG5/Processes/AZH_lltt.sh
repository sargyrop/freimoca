#!/usr/bin/env bash

procName=$1
mA=$2
mH=$3
widthA=$4
widthH=$5
tanb=$6
runMS=$7
outDir=$8

NEVENTS=100000

# if [[ "$widthA" == "0.001" ]] ; then
#   width="nw"
# else
#   width="lw"
# fi

procDir=$procName'_'$mA'_'$mH'_tanb'$tanb
paramCard=$outDir/$procDir/Cards/param_card.dat
runCard=$outDir/$procDir/Cards/run_card.dat

# Function to make process card
make_proc_card() {
  echo "import model Pseudoscalar_2HDM" > proc
  echo "generate g g > h3 > h2 z [QCD]" >> proc
  echo "output $outDir/$procDir -f" >> proc
}

# Update run card
update_cards() {
  ## Top mass
  sed -i "s|6 1.720000e+02|6 1.725000e+02|" $paramCard
  # A mass
  sed -i "s|36 6.000000e+02|36 $mA|" $paramCard
  # H mass
  sed -i "s|35 5.000000e+02|35 $mH|" $paramCard
  # Charged Higgs mass set equal to mA (mh3)
  sed -i "s|37 7.000000e+02|37 $mA|" $paramCard
  # A width
  sed -i "s|DECAY  36 2.000000e+00|DECAY  36 $widthA|" $paramCard
  # H width
  sed -i "s|DECAY  35 2.000000e+00|DECAY  35 $widthH|" $paramCard
  # H+- width
  sed -i "s|DECAY  37 2.000000e+00|DECAY  37 $widthA|" $paramCard
  # Tan(beta)
  sed -i "s|2 2.000000e+00 # tanbeta|2 $tanb # tanbeta|" $paramCard
  # Sin(beta-alpha)
  sed -i "s|3 5.000000e-01 # sinbma|3 1.0 # sinbma|" $paramCard
  # Lambdas
  sed -i "s|1 2.000000e+00 # lam3|1 3 # lam3|" $paramCard
  sed -i "s|2 3.000000e+00 # laP1|2 3 # laP1|" $paramCard
  sed -i "s|3 4.000000e+00 # laP2|3 3 # laP2|" $paramCard
  # sin(theta) A/a mixing angle
  sed -i "s|5 7.071070e-01 # sinp|5 0 # sinp|" $paramCard
  
  # Run parameters
  sed -i "s|10000 = nevents|$NEVENTS = nevents|" $runCard
  sed -i "s|nn23lo1    = pdlabel|lhapdf    = pdlabel|" $runCard
  sed -i "s|230000    = lhaid|260000    = lhaid|" $runCard
  sed -i "s|True  = use_syst|False  = use_syst|" $runCard
  
  echo "launch $outDir/$procDir -f" > launch
}

make_ms_card() {
  mode=$1
  if [[ "$mode" == "top" ]] ; then
    dir=run_01
  else
    dir=run_02
  fi
  echo "import $outDir/$procDir/Events/$dir/unweighted_events.lhe.gz" > ms_card
  echo "set spinmode none" >> ms_card
  if [[ "$mode" == "top" ]] ; then
    echo "decay z > l+ l-" >> ms_card
    echo "decay h2 > t t~, t > w+ b, w+ > l+ vl, t~ > w- b~, w- > j j" >> ms_card
  else
    echo "decay z > l+ l-" >> ms_card
    echo "decay h2 > t t~, t > w+ b, w+ > j j, t~ > w- b~, w- > l- vl~" >> ms_card
  fi
  echo "launch" >> ms_card
}

# Setup for MG
FREIMOCADIR=$HOME/xxl/freimoca
if [ ! -d $FREIMOCADIR ] ; then
  echo "Directory: $FREIMOCADIR does not exist"
  exit 1
fi
cd $FREIMOCADIR
. setup.sh MG
echo "Set up MadGraph"

# Directory where MG is located
MGDIR=$FREIMOCADIR/MG5_aMC_v2_8_1

if [ ! -d $MGDIR ] ; then
  echo "Directory: $MGDIR does not exist"
  exit 1
fi

# Make temporary directory and copy madgraph dir 
TMPDIR=$(mktemp -d)
cd $TMPDIR
echo "In tmp directory: $TMPDIR. Copying MG..."
cp -r $MGDIR .

echo "MG copied, will now create process..."

# Generate the process
cd MG5_aMC_v2_8_1
make_proc_card
./bin/mg5_aMC ./proc

# Check if output directory has been created
if [ ! -d $outDir/$procDir ] ; then
  echo "ERROR: $outDir/$procDir does not exist"
  exit 2
fi

# Update the cards and launch generation
update_cards
cat $paramCard
cat $runCard
# We want to run twice since one file will have top decaying hadronically
# and the other will have antitop decaying hadronically
./bin/mg5_aMC ./launch
./bin/mg5_aMC ./launch

# Create madspin card
if [[ "$runMS" == "True" ]] ; then
  # Run MadSpin
  make_ms_card top
  ./MadSpin/madspin ./ms_card
  make_ms_card antitop
  ./MadSpin/madspin ./ms_card
  
  # Rename output files as appropriate so that they can be picked up by other scripts
  cd $outDir/$procDir/Events
  cd run_01
  gunzip unweighted_events_decayed.lhe.gz 
  mv unweighted_events_decayed.lhe $procDir'_1'.events
  tar czf $procDir'_1'.events.tar.gz $procDir'_1'.events
  rm $procDir'_1'.events
  cd ../run_02
  gunzip unweighted_events_decayed.lhe.gz 
  mv unweighted_events_decayed.lhe $procDir'_2'.events
  tar czf $procDir'_2'.events.tar.gz $procDir'_2'.events
  rm $procDir'_2'.events
fi

# Clean up
cd $HOME && rm -rf $TMPDIR

exit 0
