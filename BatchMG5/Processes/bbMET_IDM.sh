#!/usr/bin/env bash

proc=$1
mA=$2
mH=$3
widthA=$4
widthH=$5
runMS=$6
outDir=$7

NEVENTS=100000

if [[ "$widthA" == "0.001" ]] ; then
  width="nw"
else
  width="lw"
fi

procDir=$proc'_'$mA'_'$mH'_'$width
paramCard=$outDir/$procDir/Cards/param_card.dat
runCard=$outDir/$procDir/Cards/run_card.dat

# Function to make process card
make_proc_card() {
  echo "import model InertDoublet" > proc
  echo "generate p p  > h h2 h3" >> proc
  echo "output $outDir/$procDir -f" >> proc
}

# Update run card
update_cards() {
  # Top mass
  sed -i "s|6 1.750000e+02|6 1.725000e+02|" $paramCard
  # h mass
  sed -i "25 1.000000e+02|25 125|" $paramCard
  # A mass
  sed -i "s|36 1.200000e+05|36 $mA|" $paramCard
  # H mass
  sed -i "s|35 7.000000e+04|35 $mH|" $paramCard
  # Charged Higgs mass set equal to mA
  sed -i "s|37 2.000000e+05|37 $mA|" $paramCard
  # A width
  sed -i "s|DECAY  36 1.000000e+00|DECAY  36 $widthA|" $paramCard
  # H+- width
  sed -i "s|DECAY  37 1.000000e+00|DECAY  37 $widthA|" $paramCard
  
  # Run parameters
  sed -i "s|10000 = nevents|$NEVENTS = nevents|" $runCard
  sed -i "s|nn23lo1    = pdlabel|lhapdf    = pdlabel|" $runCard
  sed -i "s|230000    = lhaid|260000    = lhaid|" $runCard
  sed -i "s|True  = use_syst|False  = use_syst|" $runCard
  
  echo "launch $outDir/$procDir -f" > launch
}

# Setup for MG
FREIMOCADIR=$HOME/xxl/freimoca
if [ ! -d $FREIMOCADIR ] ; then
  echo "Directory: $FREIMOCADIR does not exist"
  exit 1
fi
cd $FREIMOCADIR
. setup.sh MG
echo "Set up MadGraph"

# Directory where MG is located
MGDIR=$FREIMOCADIR/MG5_aMC_v2_8_1

if [ ! -d $MGDIR ] ; then
  echo "Directory: $MGDIR does not exist"
  exit 1
fi

# Make temporary directory and copy madgraph dir 
TMPDIR=$(mktemp -d)
cd $TMPDIR
echo "In tmp directory: $TMPDIR. Copying MG..."
cp -r $MGDIR .

echo "MG copied, will now create process..."

# Generate the process
cd MG5_aMC_v2_8_1
make_proc_card
./bin/mg5_aMC ./proc

# Check if output directory has been created
if [ ! -d $outDir/$procDir ] ; then
  echo "ERROR: $outDir/$procDir does not exist"
  exit 2
fi

# Update the cards and launch generation
update_cards
cat $paramCard
cat $runCard
./bin/mg5_aMC ./launch

# Copy output
cd $outDir/$procDir/Events
cd run_01
gunzip unweighted_events.lhe.gz 
mv unweighted_events.lhe $procDir'_1'.events
tar czf $procDir'_1'.events.tar.gz $procDir'_1'.events
rm $procDir'_1'.events

# Clean up
cd $HOME && rm -rf $TMPDIR

exit 0
