#!/usr/bin/env bash

PROCNAME=$1
NEVENTS=$2
SEED=$3
RUNNUMBER=$4
GRIDPACKDIR=$5

OUTDIR=$GRIDPACKDIR/Events/run_$RUNNUMBER

# Setup for MG
FREIMOCADIR=$HOME/xxl/freimoca
if [ ! -d $FREIMOCADIR ] ; then
  echo "Directory: $FREIMOCADIR does not exist"
  exit 1
fi
cd $FREIMOCADIR
. setup.sh MG
echo "Set up MadGraph"

# Directory where MG is located
MGDIR=$FREIMOCADIR/MG5_aMC_v2_7_3
if [ ! -d $MGDIR ] ; then
  echo "Directory: $MGDIR does not exist"
  exit 1
fi

# Check if gridpack exists
PROCDIR=$(echo $GRIDPACKDIR | awk -F'/' '{print $NF}')
GRIDPACK=$GRIDPACKDIR/../$PROCDIR.tar.gz
if [ ! -d $GRIDPACKDIR ] ; then
  echo "$GRIDPACKDIR does not exist"
  exit 2
fi 
if [ ! -f $GRIDPACK ] ; then
  echo "$GRIDPACK does not exist"
  exit 3
fi 


# Make temporary directory
TMPDIR=$(mktemp -d)
cd $TMPDIR

# Copy the gridpack
cp $GRIDPACK .
tar zxf $GRIDPACK

# Update the cards
cd $PROCDIR
sed -i "s|0 = nevents|$NEVENTS = nevents|" Cards/run_card.dat
sed -i "s|0    = iseed|$SEED = iseed|" Cards/run_card.dat

# Update the number of cores
sed -i "s|# run_mode = 2|run_mode = 0|"  Cards/amcatnlo_configuration.txt

# Generate the process
./bin/generate_events -f -p -o -x -n run_$RUNNUMBER

# Check if the output directory exists
if [ ! -d Events/run_$RUNNUMBER ] ; then
  echo "Output directory not created"
  ls Events/
  exit 3
fi

# Copy output directory
cp -a Events/run_$RUNNUMBER/ $OUTDIR

# Clean up
cd $HOME && rm -rf $TMPDIR

exit 0
