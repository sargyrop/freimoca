This directory contains a list of input files separated by analysis and sample to be used in conjuction
with `ShowerAndRivetScripts`.

## Instructions

1.  Make a list of files to download under `FileHandling/Input/ANALYSISNAME/SAMPLENAME.txt`. These files should contain a list of **input datasets - not files!**
2.  Dowload files using a replication request from rucio-ui.cern.ch  
3.  Make a list of the files stored on DESY-HH_LOCALGROUPDISK using `./getPathsRSE` (need to have rucio setup)
