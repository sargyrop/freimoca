#!/usr/bin/env bash

# For colourful printout
source helpers.sh

if [[ "$#" != "1" ]] ; then 
  printError "Usage: ./getPathsRSE.sh EVNT|LHE"
  exit 1
else
  TYPE=$1
fi

# Array of analyses for which output files should be written
ANALYSES=(AZH_lltt AZH_vvbb)

# Loop over analyses
for analysis in "${ANALYSES[@]}" ; do 

  printInfo "Finding samples for analysis: $analysis..."

  # Get all samples for this analysis
  SAMPLES=($(ls Input/$TYPE/$analysis/*.txt | awk -F'/' '{print $NF}' | sed 's|.txt||'))

  # Loop over samples in analysis
  for sample in "${SAMPLES[@]}" ; do
    
    printInfo -f "\tFinding files for sample: $sample"
    
    file=Input/$TYPE/$analysis/$sample.txt
    outfile=Output/$TYPE/$analysis/$sample.txt
    
    # If the output file is there, overwrite it
    if [ -f $outfile ] ; then rm -f $outfile ; fi
    
    # Read lines in input file (each line should correspond to an input dataset)
    # Input files also work but the script would be MUCH slower
    while read line ; do 
      if [[ $line == *"#"* ]] ; then
        continue
      else
        #rucio list-file-replicas $line | grep LOCALGROUPDISK | awk 'BEGIN {FS="/pnfs"} ; {printf "dcap://dcdcap01.usatlas.bnl.gov:22125/pnfs%s\n",$2}' >> $outfile
        rucio list-file-replicas $line | grep 'DESY-HH_LOCALGROUPDISK' | awk 'BEGIN {FS="/pnfs"} ; {printf "/pnfs%s\n",$2}' | sed 's/|//g' >> $outfile
      fi  
    done < $file

    # Remove empty lines from outputfile
    sed -i '/^$/d' $outfile

    printGood -f "\t\tFound $(wc -l $outfile | awk '{print $1}') files for sample $sample"
  done
      
done

printGood "Done"

exit 0
