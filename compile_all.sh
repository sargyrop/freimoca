#!/bin/bash

if [[ $_ == $0 ]] ; then
  echo "You need to source the script"
  exit 1
fi

BASEDIR=$PWD

# Set up latest gcc version needed for compilation
. ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "gcc gcc620_x86_64_slc6" 
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"

# Download madgraph - no need to compile
wget "https://launchpad.net/mg5amcnlo/lts/2.7.x/+download/MG5_aMC_v2.7.3.tar.gz"
tar zxf MG5_aMC_v2.7.3.tar.gz ; rm -f MG5_aMC_v2.7.3.tar.gz

# Download and install LHAPDF
wget "https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.3.0.tar.gz" -O LHAPDF-6.3.0.tar.gz
tar xf LHAPDF-6.3.0.tar.gz ; rm -f LHAPDF-6.3.0.tar.gz
cd LHAPDF-6.3.0
./configure --prefix=$PWD/gcc620_x86_64-slc6
make -j12 && make install
if [[ "$?" != "0" ]] ; then echo "Compilation of LHAPDF failed"; return 1; fi
export PATH=$PATH:$PWD/gcc620_x86_64-slc6/bin
cd $BASEDIR

# First download and compile YODA
curl "https://yoda.hepforge.org/downloads/?f=YODA-1.8.0.tar.gz" --output YODA-1.8.0.tar.gz
tar zxf YODA-1.8.0.tar.gz ; rm -f YODA-1.8.0.tar.gz
cd YODA-1.8.0
./configure --prefix=$PWD/gcc620_x86_64-slc6 --enable-root
make -j12 && make install
if [[ "$?" != "0" ]] ; then echo "Compilation of YODA failed"; return 1; fi
cd $BASEDIR

# Then download and compile fastjet
curl http://fastjet.fr/repo/fastjet-3.3.3.tar.gz --output fastjet-3.3.3.tar.gz
tar zxf fastjet-3.3.3.tar.gz ; rm -f fastjet-3.3.3.tar.gz
cd $BASEDIR/fastjet-3.3.3
./configure --enable-allcxxplugins --prefix=$PWD/gcc620_x86_64-slc6
make -j12 && make install
cd $BASEDIR

# Then download and compile fjcontrib
wget http://fastjet.hepforge.org/contrib/downloads/fjcontrib-1.044.tar.gz
tar zxf fjcontrib-1.044.tar.gz ; rm -f fjcontrib-1.044.tar.gz
cd $BASEDIR/fjcontrib-1.044
./configure --prefix=$PWD/gcc620_x86_64-slc6 --fastjet-config=$BASEDIR/fastjet-3.3.3/gcc620_x86_64-slc6/bin/fastjet-config
make -j12 && make install
make fragile-shared-install
if [[ "$?" != "0" ]] ; then echo "Compilation of fjcontrib failed"; return 1; fi
cd $BASEDIR

# Then download and compile HepMC2
mkdir HepMC2 ; mkdir HepMC2/build
cd $BASEDIR/HepMC2
wget http://hepmc.web.cern.ch/hepmc/releases/hepmc2.06.09.tgz
tar zxf hepmc2.06.09.tgz ; rm -f hepmc2.06.09.tgz
cd $BASEDIR/HepMC2/build
export LD_PRELOAD=/usr/lib64/libstdc++.so.6
cmake -DCMAKE_INSTALL_PREFIX=$BASEDIR/HepMC2/build/gcc620_x86_64-slc6 \
-Dmomentum:STRING=GEV \
-Dlength:STRING=MM \
../hepmc2.06.09
make -j12 && make install
if [[ "$?" != "0" ]] ; then echo "Compilation of HEPMC failed"; return 1; fi
cd $BASEDIR

# Then download and compile RIVET
curl "https://rivet.hepforge.org/downloads/?f=Rivet-3.1.0.tar.gz" --output Rivet-3.1.0.tar.gz
tar zxf Rivet-3.1.0.tar.gz ; rm -f Rivet-3.1.0.tar.gz
cd $BASEDIR/Rivet-3.1.0
./configure --prefix=$PWD/gcc620_x86_64-slc6 \
--with-yoda=$BASEDIR/YODA-1.8.0/gcc620_x86_64-slc6 \
--with-fastjet=$BASEDIR/fastjet-3.3.3/gcc620_x86_64-slc6 \
--with-fjcontrib=$BASEDIR/fjcontrib-1.044/gcc620_x86_64-slc6 \
--with-hepmc=$BASEDIR/HepMC2/build/gcc620_x86_64-slc6
make -j12 && make install
if [[ "$?" != "0" ]] ; then echo "Compilation of Rivet failed"; return 1; fi
cd $BASEDIR

# Download and compile pythia
wget https://www.pythia.org/download/pythia83/pythia8303.tgz
tar xfz pythia8303.tgz ; rm pythia8303.tgz
cd pythia8303
./configure --prefix=$PWD/gcc620_x86_64-slc6 \
--with-fastjet3=$BASEDIR/fastjet-3.3.3/gcc620_x86_64-slc6 \
--with-hepmc2=$BASEDIR/HepMC2/build/gcc620_x86_64-slc6 \
--with-lhapdf6=$BASEDIR/LHAPDF-6.3.0/gcc620_x86_64-slc6 \
--with-rivet=$BASEDIR/Rivet-3.1.0/gcc620_x86_64-slc6 \
--with-yoda=$BASEDIR/YODA-1.8.0/gcc620_x86_64-slc6 \
--with-root=/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt
make -j12 && make install
if [[ "$?" != "0" ]] ; then echo "Compilation of Pythia failed"; return 1; fi
cd $BASEDIR

echo "compile_all.sh script finished"
