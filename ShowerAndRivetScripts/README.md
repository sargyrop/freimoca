# ShowerAndRivetScripts

Contains custom `RIVET` analyses and scripts to run those analysis on a batch cluster in two ways:
- in the ATLAS software framework (`athena`) 
- using the standalone installation provided with `freimoca`


### Rivet analyses  

The Rivet analyses are located in `RivetRoutines/`. 

### Pythia routines

Custom Pythia routines to perform showering of LHE files can be found in `PythiaRoutines/`. These have to be transferred (together with
the (`Makefile`) in the `examples` directory of the locally installed Pythia8 programme and compiled there.


#### How to run shower and Rivet 

```
cd BatchScripts
./submit.py -a ANALYSIS
```


#### Compilation

To comiple a custom `RIVET` analysis do

```
cd RivetRoutines
./compile.sh AZH/AZH.cc 
```



#### Running

To run a RIVET analysis using an `EVNT` file as input, do  

```
cd BatchScripts
./runRivet.sh $PWD/../../../FileHandling/Output/EVNT/HWWAnalysis/input.txt $PWD/../../RivetRoutines/HWWAnalysis $PWD/../output_ROOT 0
```


#### Plotting

For plotting one can either use the functionality that comes with RIVET/YODA (e.g. `rivet-mkhtml`, see [here](https://rivet.hepforge.org/trac/wiki/RivetHistogramming)) or use ROOT.  

To use ROOT go to the base directory and do  
```
source setup.sh
```
in order to put `yoda2root` to the `PATH` variable.  

Then running `yoda2root` giving as input any `*.yoda` or `*.yoda.gz` file will create a ROOT file, e.g.
```
yoda2root HIST.19990447_002207.yoda.gz
```  
will create `HIST.19990447_002207.yoda.gz.root`, which can be opened and processed in a ROOT macro.
