#!/bin/zsh

while true; do
  case "$1" in
    --mode=*)      mode=${1#*=};         shift ;;
    --codedir=*)   CODEDIR=${1#*=};      shift ;;
    --inputfile=*) INPUTFILE=${1#*=};    shift ;;
    --outputdir=*) OUTPUTDIR=${1#*=};    shift ;;
    --smear=*)     DOSMEAR=${1#*=};      shift ;;
    --rivetAnal=*) ANALYSIS=${1#*=};     shift ;;
    --sample=*)    SAMPLE=${1#*=};       shift ;;
    --chunk=*)     CHUNK=${1#*=};        shift ;;
    --events=*)    MAXEVENTS=${1#*=};    shift ;;
    --rivetName=*) RIVETRUNNAME=${1#*=}; shift ;;
    --rivetOpt=*)  RIVETOPTIONS=${1#*=}; shift ;;
    --showerExe=*) SHOWEREXE=${1#*=};    shift ;;
    --showerOpt=*) SHOWEROPT=${1#*=};    shift ;;
    * ) break ;;
  esac
done

echo "--------------"
echo "mode         = $mode"
echo "CODEDIR      = $CODEDIR"
echo "INPUTFILE    = $INPUTFILE"
echo "OUTPUTDIR    = $OUTPUTDIR"
echo "DOSMEAR      = $DOSMEAR"
echo "ANALYSIS     = $ANALYSIS"
echo "SAMPLE       = $SAMPLE" 
echo "CHUNK        = $CHUNK"  
echo "MAXEVENTS    = $MAXEVENTS"
echo "RIVETRUNNAME = $RIVETRUNNAME"
echo "RIVETOPTIONS = $RIVETOPTIONS"
echo "SHOWEREXE    = $SHOWEREXE"
echo "SHOWEROPT    = $SHOWEROPT"
echo "--------------"

# Function to run the shower routine (shower.py)
# It assumes that the INPUTFILE is a path to a single file
run_shower() {
               
  # Copy the input file
  echo "Copying $(cat $INPUTFILE)"
  cp $(cat $INPUTFILE) .
  filename=$(cat $INPUTFILE | awk -F'/' '{print $NF}')
  runname=$(cat $INPUTFILE | awk -F'/' '{print $(NF-1)}') 
  basefilename=$(echo $filename | awk -F'.' '{print $1}')"_$runname"
  echo "filename=$filename"
  echo "basefilename=$basefilename"
   
  # If file is not a tar arxiv (default of MG5 gunzip it and fix naming)
  file $filename | grep -q 'tar archive'
  if [[ "$?" == "1" ]] ; then
    echo "Unzipping $filename"
    gunzip $filename
    
    # rename the original file and the tarball
    echo "Renaming $(ls *.lhe) to $basefilename.events"
    mv $(ls *.lhe) $basefilename.events    
    filename=$basefilename.events   
  fi 
          
  # run the shower
  echo "Copying $SHOWEREXE"
  echo "Copying $SHOWEROPT"
  cp $SHOWEREXE .
  cp $SHOWEROPT .
  showerExeFile=$(basename $SHOWEREXE)
  showerOptFile=$(basename $SHOWEROPT)
  if [ ! -f $showerExeFile -o ! -f $showerOptFile ] ; then
    echo "ERROR: shower files not copied"
    exit 1
  fi
  
  # Fix cmnd file so that it takes correct input file
  origLHEFile=$(grep "Beams:LHEF" $showerOptFile | awk -F'=' '{print $2}' | awk '{print $1}')
  echo "Replace input file: $origLHEFile with $filename"
  sed -i "s/$origLHEFile/$filename/" $showerOptFile
  
  # Fix number of events to run on
  if [[ "$MAXEVENTS" == "-1" ]] ; then
    echo "ERROR: cannot run pythia with -1 events"
    exit 3
  fi
  origEvents=$(grep "Main:numberOfEvents" $showerOptFile | awk -F'=' '{print $2}' | awk '{print $1}')
  sed -i "s/Main:numberOfEvents = $origEvents/Main:numberOfEvents = $MAXEVENTS/" $showerOptFile
      
  echo "Run: ./$showerExeFile $showerOptFile HEPMC.$CHUNK.hepmc >& log.generate"
  ./$showerExeFile $showerOptFile HEPMC.$CHUNK.hepmc >& log.generate
    
  if [ $? -ne 0 ] ; then
    cp log.generate $CODEDIR/logs
    echo "ERROR: Problem running the shower. HepMC file not generated. Aborting"
    exit 2
  fi
    
  if [ ! -d $OUTPUTDIR ] ; then
    mkdir $OUTPUTDIR
  fi
  
  echo "Copy to $OUTPUTDIR/HEPMC.$CHUNK.hepmc"
  cp HEPMC.$CHUNK.hepmc $OUTPUTDIR/HEPMC.$CHUNK.hepmc
}

# Function to run the rivet routine 
# It assumes that the INPUTFILE is a file containing paths to multiple EVNT files
run_rivet() {
  
  # Copy the input file
  echo "Copying $(cat $INPUTFILE)"
  cp $(cat $INPUTFILE) .
  filename=$(cat $INPUTFILE | awk -F'/' '{print $NF}')
  echo "filename=$filename"
  
  if [ ! -f $filename ] ; then
    echo "ERROR copying $filename"
    exit 1
  fi
  
  # Export analysis path
  export RIVET_ANALYSIS_PATH=$CODEDIR/../RivetRoutines/$ANALYSIS
  echo "RIVET_ANALYSIS_PATH=$RIVET_ANALYSIS_PATH"
  
  # Run rivet
  if $DOSMEAR ; then RIVETOPTIONS="$RIVETOPTIONS:SMEAR=ON" ; fi
  if [ ! -z $SAMPLE ] ; then RIVETOPTIONS="$RIVETOPTIONS:SAMPLE=$SAMPLE" ; fi
  
  if [[ "$MAXEVENTS" != "-1" ]] ; then ADDCOMMAND="-n $MAXEVENTS" ; else ADDCOMMAND="" fi
  
  echo "Run: rivet $filename -a $ANALYSIS$RIVETOPTIONS $ADDCOMMAND"
  rivet $filename -a $ANALYSIS$RIVETOPTIONS $ADDCOMMAND
  
  # Check if the output has been created
  OUTPUT="Rivet.yoda"
  if [ ! -f $OUTPUT ] ; then
    echo "ERROR no yoda files have been created"
    exit 1
  else
    if [ ! -d $OUTPUTDIR ] ; then
      mkdir -p $OUTPUTDIR
    fi
  
    echo "Copy to $OUTPUTDIR/$SAMPLE'_'$CHUNK.yoda"
    cp $OUTPUT $OUTPUTDIR/$SAMPLE'_'$CHUNK.yoda

    # Copy yoda2root conversion file
    cp $CODEDIR/yoda2root.py .
    ./yoda2root.py $OUTPUT
    ROOTOUTPUT=$(ls *.root)
    if [ -z $ROOTOUTPUT ] ; then
      echo "ERROR yoda2root conversion failed"
      exit 2
    fi
    # Move root file to destination
    mv $ROOTOUTPUT $OUTPUTDIR/$SAMPLE'_'$CHUNK.root

  fi
}

# Main script

# setup environment  
. $CODEDIR/../../setup.sh all

# A temporary directory is made where all input file and scripts will be stored
TMPDIR=$(mktemp -d)
cd $TMPDIR
     
echo "We are in $PWD"
  
if [[ "$mode" == "1" ]] ; then  
  run_shower
elif [[ "$mode" == "2" ]] ; then  
  run_rivet
elif [[ "$mode" == "3" ]] ; then  
  run_shower
  run_rivet
fi

# clean up
echo "Cleaning up..." 
cd $CODEDIR
rm -rf $TMPDIR
echo "Done..."

exit 0
   
