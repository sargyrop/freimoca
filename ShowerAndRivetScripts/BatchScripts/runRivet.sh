#!/bin/bash

if [[ "$#" < 5 ]] ; then
  echo "Usage: ./runRivet.sh EVNT_FILE RIVET_ANALYSIS_PATH DESTINATION DOSMEAR SAMPLENAME RUNNAME OPTIONS"
  exit 1
fi

INPUTEVNTPATH=$1
ANALYSISPATH=$2
DESTINATION=$3
DOSMEAR=$4
SAMPLE=$5
RUNNAME=$6
OPTIONS=$7
INPUTEVNTFILE=$(basename $INPUTEVNTPATH)
ANALYSISNAME=$(basename $ANALYSISPATH)

# Print settings
echo "Start at: $(date)"
echo "Running with..."
echo "INPUTEVNTPATH = $INPUTEVNTPATH"
echo "ANALYSISPATH  = $ANALYSISPATH"
echo "DESTINATION   = $DESTINATION"
echo "DOSMEAR       = $DOSMEAR"
echo "SAMPLE        = $SAMPLE"
echo "RUNNAME       = $RUNNAME"
echo "OPTIONS       = $OPTIONS"
echo "---------------"

if [ ! -f "$INPUTEVNTPATH" ]; then
  echo "ERROR: $INPUTEVNTPATH does not exist"
  exit 2
fi

if [ ! -d "$ANALYSISPATH" ] ; then
  echo "Rivet analysis directory: $ANALYSISPATH does not exist"
  exit 3
fi

if [ ! -f "$ANALYSISPATH/Rivet$ANALYSISNAME.so" ] ; then
  echo "Rivet shared library: $ANALYSISPATH/Rivet$ANALYSISNAME.so not found!"
  echo "Run "
  echo "./compile.sh $ANALYSISPATH"
  exit 4
fi

if [ ! -z "$OPTIONS" -a -z "$RUNNAME" -o -z "$OPTIONS" -a ! -z "$RUNNAME" ] ; then
  echo "You have to specify a RUNNAME for Rivet when running with OPTIONS"
  exit 5
fi

if [ ! -d $DESTINATION ] ; then
  mkdir $DESTINATION
fi


###################
# Global settings #
###################
RELEASE=AthGeneration,23.6.11
SCRIPTDIR=$PWD
SCRIPT=runRivetAnalysisOnEVNT.py

######################
# set up environment #
######################

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# Set up athena release
asetup $RELEASE

# Set up rivet
. setupRivet

########################
# set up run directory #
########################

# Create a temporary directory where we will run Rivet
TMPDIR=$(mktemp -d) && cd $TMPDIR

# Copy EVNT files
cp $INPUTEVNTPATH .
while read line; do 
  echo "Copying: $line"
  cp $line .
  file=$(basename $line)
  if [ ! $file ] ; then 
    echo "ERROR copying $file"
    exit 6
  fi 
done < $INPUTEVNTPATH

# Copy jobOption
cp $SCRIPTDIR/$SCRIPT .
if [ ! -f $SCRIPT ] ; then
  echo "ERROR copying $SCRIPT"
  exit 7
fi

#######
# Run #
#######

# First fix the jobOption
sed -i "s|XXXANALYSIS|$ANALYSISNAME|"   $SCRIPT
sed -i "s|XXXPATH|$ANALYSISPATH|"       $SCRIPT
sed -i "s|XXXINPUTFILE|$INPUTEVNTFILE|" $SCRIPT
if [[ "$DOSMEAR" == "true" ]] ; then
  sed -i "s|XXXDOSMEAR|True|" $SCRIPT
else
  sed -i "s|XXXDOSMEAR|False|" $SCRIPT
fi
if [ ! -z $SAMPLE ] ; then
  sed -i "s|XXXSAMPLENAME|$SAMPLE|" $SCRIPT
fi
if [ ! -z $OPTIONS ] ; then
  sed -i "s|XXXOPTIONS|$OPTIONS|" $SCRIPT
else
  sed -i "s|XXXOPTIONS||" $SCRIPT
fi

# Run the jobOption with athena
athena $SCRIPT

###################
# Transfer output #
###################

# Check if the output has been created
OUTPUT=$(ls *.yoda*)
if [ -z $OUTPUT ] ; then
  echo "ERROR no yoda files have been created"
  exit 8
else
  # Copy yoda file
  cp $OUTPUT $DESTINATION
  
  gunzip $OUTPUT
  # Copy yoda2root conversion file
  cp $ANALYSISPATH/../../BatchScripts/yoda2root.py .
  ./yoda2root.py $(echo $OUTPUT | sed 's|.gz||')
  ROOTOUTPUT=$(ls HIST*.root)
  if [ -z $ROOTOUTPUT ] ; then
    echo "ERROR yoda2root conversion failed"
    exit 9
  fi
  # Move root file to destination
  mv $ROOTOUTPUT $DESTINATION
  
  # Move csv files if they exist
  CSVFILE=$(ls *.csv)
  CSVFILENAME=$(echo $ROOTOUTPUT | sed 's/HIST.//' | sed 's/.root/.csv/')
  if [ ! -z $CSVFILE ] ; then 
    mv $CSVFILE $DESTINATION/$CSVFILENAME
  fi
fi

# Clean up
cd $SCRIPTDIR
rm -rf $TMPDIR

echo "End at: $(date)"

exit 0
