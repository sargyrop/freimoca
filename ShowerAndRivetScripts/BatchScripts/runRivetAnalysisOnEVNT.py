import glob

# Global settings
ANALYSIS="XXXANALYSIS"     # Analysis name
PATH="XXXPATH"             # Path to directory where RivetAnalysis.so is located
INPUTFILE="XXXINPUTFILE"   # Input EVNT file
SAMPLENAME="XXXSAMPLENAME" # sample name
doSmear=XXXDOSMEAR         # Smearing
OPTIONS="XXXOPTIONS"       # Optional arguments for Rivet run
RUNNAME="XXXRUNNAME"       # Rivet run name (optional)

# The input file will be named EVNT.tag.pool.root
if INPUTFILE.startswith("submitData"):
	OUTPUTFILE=INPUTFILE.strip("submitData_")
else:
	OUTPUTFILE=INPUTFILE.strip("EVNT.").strip(".root").strip(".pool")

# How many events to run (-1 = all events in EVNT file)
theApp.EvtMax = -1

import AthenaPoolCnvSvc.ReadAthenaPool
# For running on single input file
#svcMgr.EventSelector.InputCollections = [ INPUTFILE ]
# For running on many files
svcMgr.EventSelector.InputCollections = glob.glob("EVNT.*root*")

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = PATH

# Smearing 
if doSmear: 
	SMEAR=":SMEAR=ON"
else:
	SMEAR=""
SAMPLE=":SAMPLE="+SAMPLENAME

rivet.Analyses += [ ANALYSIS+SMEAR+SAMPLE+OPTIONS ]
rivet.RunName = RUNNAME
rivet.HistoFile = 'HIST.{OUTPUTFILE}.yoda.gz'.format(OUTPUTFILE=OUTPUTFILE)
rivet.CrossSection = 1.0
rivet.SkipWeights=True
job += rivet
