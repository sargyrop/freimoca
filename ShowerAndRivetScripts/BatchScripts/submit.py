#!/usr/bin/env python3

import os, re, sys, getpass, glob

from argparse import ArgumentParser
from os.path import join as pjoin
from os import getcwd
from collections import namedtuple
from itertools import zip_longest

# Arguments
parser = ArgumentParser(usage=__doc__)
parser.add_argument("-a", "--analysis", required=True, dest="ANALYSIS",  help="Specify which analysis to run")
parser.add_argument("-t", "--test",     action="store_true", default=False, dest="TEST",  help="only submit single job per sample")
parser.add_argument("-c", "--check",    action="store_true", default=False, dest="CHECK", help="check jobs which have finished without submitting new jobs")
parser.add_argument("-m", "--merge",    action="store_true", default=False, dest="MERGE", help="merge output root files in a single file stored in ../output_ROOT")
args = parser.parse_args()

# Named tuple to hold setups to run
Setup=namedtuple('Setup', ['ANALYSIS',
                           'SAMPLE', 
						   'RUNTYPE', 
                           'STANDALONE', 
 						   'NGROUP', 
						   'SMEAR', 
						   'EVENTS', 
						   'RIVETRUNNAME', 
						   'RIVETOPTIONS',
						   'SHOWEREXE',
						   'SHOWEROPT'])
Setup.__new__.__defaults__ = (None,)*11

# A function to write out the files that will be read by a single job
def dump(contents,outputFile):
	with open(outputFile,'w') as file:
		file.write(contents)

# The function that submits the shower/rivet jobs
def submit(setup):
	# Different modes for shower/rivet running
	MODESETTING={"SO":"1", "RO":"2", "SR":"3"}
	
	if setup.RUNTYPE not in MODESETTING:
		print(f"ERROR: unknown running mode: {setup.RUNTYPE}. Possible options are SO,RO,SR")
		sys.exit(1)
	
	# Where the shower scripts are located
	CODEDIR=getcwd()

	# Where the LHE or EVNT files are located
	if setup.RUNTYPE == "SO":
		inputFilePath=f"{getcwd()}/../../FileHandling/Output/LHE/{setup.SAMPLE}/{setup.SAMPLE}.txt"
	else:
		if setup.STANDALONE:
			inputFilePath=f"{getcwd()}/../../FileHandling/Output/HEPMC/{setup.SAMPLE}/{setup.SAMPLE}.txt"
		else:
			inputFilePath=f"{getcwd()}/../../FileHandling/Output/EVNT/{setup.SAMPLE}/{setup.SAMPLE}.txt"
		
	# Where to put the output files
	if setup.RUNTYPE == "SO":
		if setup.STANDALONE:
			OUTPUTDIR=f"/nfs/dust/atlas/user/{getpass.getuser()}/HEPMC/{setup.SAMPLE}"
		else:
 			OUTPUTDIR=f"/nfs/dust/atlas/user/{getpass.getuser()}/EVNT/{setup.SAMPLE}"
	else:
		OUTPUTDIR=f"/nfs/dust/atlas/user/{getpass.getuser()}/ROOT/{setup.ANALYSIS}/{setup.SAMPLE}"
		if setup.RIVETOPTIONS: OUTPUTDIR+=f"_{setup.RIVETRUNNAME}"
		
	# Loop through the input files and submit one job per input file
	filetype="tar.gz" if setup.RUNTYPE == "SO" or setup.RUNTYPE == "SR" else "root"
    	
	# Open file containing paths to input files and start reading the paths in chunks of setup.NGROUP
	nOutputExpected = 0
	nOutputDone = 0
	with open(inputFilePath) as inputFile:
		chunk=1
		
		# For the next group of NGROUP lines do:
		for lines in zip_longest(*[inputFile] * setup.NGROUP):
			# write the lines to a file
			fileWithInputs=f'tempSubmissionFiles/submitData_{setup.ANALYSIS}_{setup.SAMPLE}_{setup.RUNTYPE}_group{setup.NGROUP}_smear{setup.SMEAR}_chunk{chunk}'
			dump("".join(i for i in lines if i is not None), fileWithInputs)
			nOutputExpected += 1
			
			jobName=f"{fileWithInputs.strip('tempSubmissionFiles/submitData_')}"
			
			# Built the arguments string
			ARGS=f"--mode={MODESETTING[setup.RUNTYPE]} "
			ARGS+=f"--codedir={CODEDIR} "
			ARGS+=f"--inputfile={getcwd()}/{fileWithInputs} "
			ARGS+=f"--outputdir={OUTPUTDIR} "
			ARGS+=f"--smear={'true' if setup.SMEAR else 'false'} "
			ARGS+=f"--rivetAnal={setup.ANALYSIS} "
			ARGS+=f"--sample={setup.SAMPLE} "
			ARGS+=f"--chunk=chunk{chunk} "
			ARGS+=f"--events={setup.EVENTS if setup.EVENTS else -1} "
			ARGS+=f"--rivetName={setup.RIVETRUNNAME} "
			ARGS+=f"--rivetOpt={setup.RIVETOPTIONS} "
			ARGS+=f"--showerExe={setup.SHOWEREXE} "
			ARGS+=f"--showerOpt={setup.SHOWEROPT} "
										    	
			# Copy file submission template to temporary file and read its contents
			os.system("cp sub sub.sh")
			with open("sub.sh", "r+") as file:
				filedata=file.read()
		
				# Replace submission commands	
				filedata = filedata.replace("XEXECUTABLE", "run_shower_rivet.sh" if not setup.STANDALONE else 
															"run_shower_rivet_standalone.sh")
				filedata = filedata.replace("XARGS",       ARGS)
				filedata = filedata.replace("USER",        getpass.getuser())				
				filedata = filedata.replace("JOBNAME",     jobName)
				filedata = filedata.replace("CODEDIR",     f"{CODEDIR}")
				
				# Replace file contents
				file.seek(0)
				file.write(filedata)
    	
			# Submit job and clean up
			if not glob.glob(f"{OUTPUTDIR}/*chunk{chunk}*"):
				if not args.CHECK: 
					os.system("condor_submit sub.sh")
				#os.remove("sub.sh")
			else:
				nOutputDone += 1
				if not args.CHECK:
					print(f"File for {jobName} exists in {OUTPUTDIR}. Skipping")
				
			# Update the chunk iterator
			chunk += 1
			
			# For debugging submit only 1 job
			if args.TEST and not args.CHECK: 
				break
	
	if args.CHECK:
		if nOutputExpected == nOutputDone:
			print(f"Sample: {setup.SAMPLE:25s} done")
		else:
			print(f"Sample {setup.SAMPLE:25s}: Expected = {nOutputExpected:4d} , Done = {nOutputDone:4d}")	


# A function to merge output root files in a single file
def merge(setup):
	INPUTDIR=f"/nfs/dust/atlas/user/{getpass.getuser()}/ROOT/{setup.ANALYSIS}/{setup.SAMPLE}"
	INPUTFILES=f"HIST.{setup.ANALYSIS}_{setup.SAMPLE}_{setup.RUNTYPE}_group{setup.NGROUP}_smear{setup.SMEAR}_chunk*.root"
	OUTPUTDIR="../output_ROOT"
	if 'AZH_lltt' in setup.SAMPLE:
		INPUTDIR=re.sub(r"nw_\d", "nw_*", INPUTDIR)
		INPUTFILES=re.sub(r"nw_\d", "nw_*", INPUTFILES)
		sample=re.sub(r"nw_\d", "nw", setup.SAMPLE)
		os.system(f"hadd -f {OUTPUTDIR}/{setup.ANALYSIS}_{sample}_smear{setup.SMEAR}.root {INPUTDIR}/{INPUTFILES}")
	else:
		os.system(f"hadd -f {OUTPUTDIR}/{setup.ANALYSIS}_{setup.SAMPLE}_smear{setup.SMEAR}.root {INPUTDIR}/{INPUTFILES}")
	
# Main function
def main():
	if args.ANALYSIS == "AZH_lltt":
		# Declare setups we want to run	
		setupsToRun=[ Setup('AZH_lltt', 'ttbar',  'RO', 10, False),
					  Setup('AZH_lltt', 'WZ', 	'RO', 20, False),
					  Setup('AZH_lltt', 'ttZ_ee', 'RO', 5,  False), 
					  Setup('AZH_lltt', 'ttZ_mm', 'RO', 5,  False), 
					  Setup('AZH_lltt', 'ttW',	'RO', 5,  False), 
					  Setup('AZH_lltt', 'ttbar',  'RO', 10, True),
					  Setup('AZH_lltt', 'WZ', 	'RO', 20, True),
					  Setup('AZH_lltt', 'ttZ_ee', 'RO', 5,  True), 
					  Setup('AZH_lltt', 'ttZ_mm', 'RO', 5,  True), 
					  Setup('AZH_lltt', 'ttW',	'RO', 5,  True), 
					]
		
		# AZH lltt Signals
		mAHvalues = [(500,400),
		     		 (550,400),  (550,450),
		     		 (600,400),  (600,450),  (600,500),
		     		 (650,400),  (650,450),  (650,500),   (650,550),
		     		 (700,400),  (700,450),  (700,500),   (700,550),   (700,600),
		     		 (750,400),  (750,450),  (750,500),   (750,550),   (750,600),
		     		 (800,400),  (800,450),  (800,500),   (800,550),   (800,600),
		     		 (900,400),  (900,450),  (900,500),   (900,600),   (900,700),
		     		 (1000,400), (1000,600), (1000,900),
		     		 (1200,400), (1200,600), (1200,900),  (1200,1100),
		     		 (1500,400), (1500,700), (1500,1000), (1500,1200), (1500,1400),
		     		 (1700,400), (1700,800), (1700,1200), (1700,1600),
		     		 (2000,400), (2000,800), (2000,1200), (2000,1800)]
		for mA,mH in mAHvalues:
			for rep in 1 , 2: 
				if os.path.isfile(f"{os.getcwd()}/../../FileHandling/Output/EVNT/AZH_lltt/AZH_lltt_{mA}_{mH}_nw_{rep}.txt"):
					setupsToRun += [ #Setup('AZH_lltt', f'AZH_lltt_{mA}_{mH}_nw_{rep}', 'SO', 1,  False), 
									 Setup('AZH_lltt', f'AZH_lltt_{mA}_{mH}_nw_{rep}', 'RO', 1,  False),
							 		 Setup('AZH_lltt', f'AZH_lltt_{mA}_{mH}_nw_{rep}', 'RO', 1,  True)
									]
				else:
					print(f"File: AZH_lltt_{mA}_{mH}_nw_{rep}.txt does not exist - skipping")
	
	elif args.ANALYSIS == "AZH_vvbb":
		# Declare setups we want to run	
		setupsToRun=[ Setup('AZH_vvbb', 'ttbar',     'RO', 5,  False),
					  Setup('AZH_vvbb', 'stop_top',  'RO', 5,  False),
					  Setup('AZH_vvbb', 'stop_atop', 'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Wenu_Np0',  'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Wenu_Np1',  'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Wenu_Np2',  'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Wenu_Np3',  'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Wenu_Np4',  'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Znunu_Np0', 'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Znunu_Np1', 'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Znunu_Np2', 'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Znunu_Np3', 'RO', 5,  False), 
					  Setup('AZH_vvbb', 'Znunu_Np4', 'RO', 5,  False),
					  Setup('AZH_vvbb', 'qqZHvvbb',  'RO', 5,  False), 					  
					  Setup('AZH_vvbb', 'ttbar',     'RO', 5,  True),
					  Setup('AZH_vvbb', 'stop_top',  'RO', 5,  True),
					  Setup('AZH_vvbb', 'stop_atop', 'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Wenu_Np0',  'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Wenu_Np1',  'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Wenu_Np2',  'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Wenu_Np3',  'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Wenu_Np4',  'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Znunu_Np0', 'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Znunu_Np1', 'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Znunu_Np2', 'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Znunu_Np3', 'RO', 5,  True), 
					  Setup('AZH_vvbb', 'Znunu_Np4', 'RO', 5,  True),
					  Setup('AZH_vvbb', 'qqZHvvbb',  'RO', 5,  True)		
					]
	
		# AZH vvbb signals
		mAHvalues = [(400,200),  (400,300),
					 (500,200),  (500,300),  (500,400),
					 (600,200),  (600,300),  (600, 400), (600, 500),
				 	 (700,200),  (700,300),  (700,400),  (700, 500),
				 	 (800,200),  (800,300),  (800,400),  (800,500),  (800,600),
				 	 (900,200),  (900,400),  (900,600),
				 	 (1000,200), (1000,400), (1000,600), (1000,800),
				 	 (1200,200), (1200,400), (1200,600), (1200,900), (1200,1100)]
		for mA,mH in mAHvalues:
			if os.path.isfile(f"{os.getcwd()}/../../FileHandling/Output/LHE/AZH_vvbb/AZH_vvbb_{mA}_{mH}_nw_1.txt"):
				setupsToRun += [ #Setup('AZH_vvbb', f'AZH_vvbb_{mA}_{mH}_nw_1', 'SO', 1,  False), 
								 Setup('AZH_vvbb', f'AZH_vvbb_{mA}_{mH}_nw_1', 'RO', 1,  False),
								 Setup('AZH_vvbb', f'AZH_vvbb_{mA}_{mH}_nw_1', 'RO', 1,  True)
						  	   ]
			else:
				print(f"File: AZH_vvbb_{mA}_{mH}_nw_1.txt does not exist - skipping")
	elif args.ANALYSIS == "FTAG":
		setupsToRun=[ Setup('FTAG', 'Zprime', 'RO', 5,  False) ]
	elif args.ANALYSIS == "bbWW":
		setupsToRun=[ Setup(ANALYSIS='ATLAS_2018_I1677498',
                            SAMPLE='bbWW_default',
                            RUNTYPE='RO',
                            STANDALONE=True,
                            NGROUP=1,
                            SMEAR=False, 
                            EVENTS=9900,
                            RIVETRUNNAME='vetotopisr_grandparent',
                            RIVETOPTIONS=':VETOTOPISR=GRANDPARENT',
							SHOWEREXE=f'{getcwd()}/../../pythia8303/examples/showerGenericToHepMC',
							SHOWEROPT=f'{getcwd()}/../../pythia8303/examples/showerGenericToHepMC.cmnd'),
#					  Setup(ANALYSIS='ATLAS_2018_I1677498',
#                            SAMPLE='bbWW_default',
#                            RUNTYPE='RO',
#                            STANDALONE=True,
#                            NGROUP=1,
#                            SMEAR=False, 
#                            EVENTS=9900,
#                            RIVETRUNNAME='vetotopisr_parent',
#                            RIVETOPTIONS=':VETOTOPISR=PARENT',
#							SHOWEREXE=f'{getcwd()}/../../pythia8303/examples/showerGenericToHepMC',
#							SHOWEROPT=f'{getcwd()}/../../pythia8303/examples/showerGenericToHepMC.cmnd'),
#					  Setup(ANALYSIS='ATLAS_2018_I1677498',
#                            SAMPLE='bbWW_default',
#                            RUNTYPE='RO',
#                            STANDALONE=True,
#                            NGROUP=1,
#                            SMEAR=False, 
#                            EVENTS=9900,
#                            RIVETRUNNAME='noveto',
#                            RIVETOPTIONS='',
#							SHOWEREXE=f'{getcwd()}/../../pythia8303/examples/showerGenericToHepMC',
#							SHOWEROPT=f'{getcwd()}/../../pythia8303/examples/showerGenericToHepMC.cmnd')							 
					]		
	else:
		print("Unknown analysis: "+args.ANALYSIS)
		sys.exit(1)
	
	# Loop over setups and submit jobs
	for runSetup in setupsToRun:
		if args.MERGE:
			merge(runSetup)
		else:
			submit(runSetup)


# If executed as a script run main function
if __name__ == "__main__":
    main()




