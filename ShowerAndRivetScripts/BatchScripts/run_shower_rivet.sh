#!/bin/zsh

hile true; do
  case "$1" in
    --mode=*)      mode=${1#*=};         shift ;;
    --codedir=*)   CODEDIR=${1#*=};      shift ;;
    --inputfile=*) INPUTFILE=${1#*=};    shift ;;
    --outputdir=*) OUTPUTDIR=${1#*=};    shift ;;
    --smear=*)     DOSMEAR=${1#*=};      shift ;;
    --rivetAnal=*) ANALYSIS=${1#*=};     shift ;;
    --sample=*)    SAMPLE=${1#*=};       shift ;;
    --chunk=*)     CHUNK=${1#*=};        shift ;;
    --events=*)    MAXEVENTS=${1#*=};    shift ;;
    --rivetName=*) RIVETRUNNAME=${1#*=}; shift ;;
    --rivetOpt=*)  RIVETOPTIONS=${1#*=}; shift ;;
    --showerExe=*) SHOWEREXE=${1#*=};    shift ;;
    --showerOpt=*) SHOWEROPT=${1#*=};    shift ;;
    * ) break ;;
  esac
done

echo "--------------"
echo "mode         = $mode"
echo "CODEDIR      = $CODEDIR"
echo "INPUTFILE    = $INPUTFILE"
echo "OUTPUTDIR    = $OUTPUTDIR"
echo "DOSMEAR      = $DOSMEAR"
echo "ANALYSIS     = $ANALYSIS"
echo "SAMPLE       = $SAMPLE" 
echo "CHUNK        = $CHUNK"  
echo "MAXEVENTS    = $MAXEVENTS"
echo "RIVETRUNNAME = $RIVETRUNNAME"
echo "RIVETOPTIONS = $RIVETOPTIONS"
echo "SHOWEREXE    = $SHOWEREXE"
echo "SHOWEROPT    = $SHOWEROPT"
echo "--------------"

# Function to run the shower routine (shower.py)
# It assumes that the INPUTFILE is a path to a single file
run_shower() {

  # A temporary directory is made where all input file and scripts will be stored
  TMPDIR=$(mktemp -d)
  echo $TMPDIR
  cd $TMPDIR
  
  # Create DSID directory
  DSIDdir=999999
  mkdir $DSIDdir
     
  # setup atlas environment  
  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase  
  . ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

  # asetup
  . /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh AthGeneration,23.6.11,here
               
  # Copy the input file to the DSID directory
  cp $(cat $INPUTFILE) $DSIDdir
  filename=$(cat $INPUTFILE | awk -F'/' '{print $NF}')
  runname=$(cat $INPUTFILE | awk -F'/' '{print $(NF-1)}') 
  basefilename=$(echo $filename | awk -F'.' '{print $1}')"_$runname"
  echo "filename=$filename"
  echo "basefilename=$basefilename"
   
  # If file is not a tar arxiv (default of MG5 gunzip it and fix naming)
  file $DSIDdir/$filename | grep -q 'tar archive'
  if [[ "$?" == "1" ]] ; then
    cd $DSIDdir
    gunzip $filename
    
    # rename the original file and the tarball
    mv events.lhe $basefilename.events    
    filename=events_$runname.tar.gz
    
    # tar it
    tar czf $filename $basefilename.events
    
    # Remove the unzipped file
    rm $basefilename.events
    cd ..
  fi 
    
  # Copy the Generate_tf command and the shower jO
  JOBOPTION=$TMPDIR/$DSIDdir/mc.aMCPy8_test.py
  cp $CODEDIR/command.txt .
  cp $CODEDIR/shower.py $JOBOPTION
  
  # update shower.py with corect Operator and Mass values
  sed -i "s|DSIDDIR|$DSIDdir|" command.txt
  sed -i "s|INPUTFILE|$DSIDdir/$filename|" command.txt
  sed -i "s|MAXEVENTS|$MAXEVENTS|" command.txt
  
  echo "----------"
  cat $JOBOPTION
  echo "----------"
  cat command.txt
  echo "----------"
  
  echo "We are in $PWD"
  
  # run the shower
  source command.txt
    
  if [ ! -f EVNT.root ] ; then
    cp log.generate $CODEDIR/logs
    echo "ERROR: Problem running the shower. EVNT file not generated. Aborting"
    exit 2
  fi
  
  grep -q "successful run" log.generate
  if [[ "$?" == "1" ]] ; then
    echo "ERROR: Not successful run. Aborting"
    cp log.generate $CODEDIR/logs/log.generate_$SAMPLE'_'$CHUNK
    exit 3
  fi
  
  if [ ! -d $OUTPUTDIR ] ; then
    mkdir $OUTPUTDIR
  fi
  
  echo "Copy to $OUTPUTDIR/EVNT.$CHUNK.root"
  cp EVNT.root $OUTPUTDIR/EVNT.$CHUNK.root

  # clean up
  echo "Cleaning up..." 
  cd ..
  rm -rf $TMPDIR
  echo "Leaving run_shower..."
}

# Function to run the rivet routine 
# It assumes that the INPUTFILE is a file containing paths to multiple EVNT files
run_rivet() {
  
  ./runRivet.sh $INPUTFILE $CODEDIR/../RivetRoutines/$ANALYSIS $OUTPUTDIR $DOSMEAR $SAMPLE $RIVETRUNNAME $RIVETOPTIONS
   
}
  
   
# Main script
if [[ "$mode" == "1" ]] ; then  
  run_shower
elif [[ "$mode" == "2" ]] ; then  
  run_rivet
elif [[ "$mode" == "3" ]] ; then  
  run_shower
  run_rivet
fi

exit 0
   
