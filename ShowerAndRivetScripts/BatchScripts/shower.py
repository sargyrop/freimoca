# A generic routine that takes an input LHE file and produces an EVNT file
# showered with Pythia 8
# Filters: semi-leptonic ttbar

#### Shower      
evgenConfig.description = 'bbWW aMC@NLO'                                                                                                                                
evgenConfig.keywords+=['top']
                              			                                                                                                                                                
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")                                                                                                               

# For LO MG
#include("Pythia8_i/Pythia8_MadGraph.py")                    

# For NLO MG
include("Pythia8_i/Pythia8_aMcAtNlo.py")

evgenConfig.generators += ["MadGraph", "Pythia8"]

#bonus_file = open('pdg_extras.dat','w')
#bonus_file.write('9000006 ForgetMe 100.0 (MeV/c) boson ForgetMe 0\n')
#bonus_file.close()
#testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'

#Pythia decays - the following is needed for special particles
pythiachans=[]
#pythiachans.append("9000006:all phinew phinew 1 0 0 0.1")
#pythiachans.append("9000006:mayDecay off")

# Modify pythia decays
pythiachans.append("24:onMode=off")
pythiachans.append("24:onIfAny 11 13")

## For debugging only: print out some Pythia config info
genSeq.Pythia8.Commands +=  ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]
genSeq.Pythia8.Commands += pythiachans

# Use semi-leptonic ttbar filter
#include('MC15JobOptions/TTbarWToLeptonFilter.py')
#filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
#filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
