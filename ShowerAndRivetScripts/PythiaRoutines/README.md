## PythiaRoutines

This directory contains some example Pythia routines that can be used for showering `LHE` events (e.g. produced by `MadGraph_aMC@NLO`)
and output `HepMC` events that can then be read with `Rivet`.

`shower_decayUH.cc` illustrates how to use a generic showering routine to produce a `HepMC` file from an input `LHE` file using the `UserHooks` class to select
specific top decays. This is useful when doing e.g. semi-leptonic top decays.

**NB: for the most generic functionality of showering a `LHE` file `main44` can be used instead, which can be found in `../../pythia8303/examples/`**

In order to compile these within the Pythia installation that comes with the package do the following:
```
cp shower_decayUH.cc.c* ../../pythia8303/examples/
cp Makefile ../../pythia8303/examples/
cd ../../pythia8303/examples/
make shower_decayUH
```
