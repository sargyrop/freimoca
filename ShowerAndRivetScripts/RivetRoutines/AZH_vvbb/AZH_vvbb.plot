# BEGIN PLOT /AZH_vvbb/MET
Title=Missing transverse energy
XLabel=$E_{T}^{miss}$ [GeV]
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}E_{T}^{miss}$ [GeV$^{-1}$]
LogY=1
# END PLOT
