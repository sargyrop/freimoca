# BEGIN PLOT /AZH_lltt/MET
Title=Missing transverse energy
XLabel=$E_{T}^{miss}$ [GeV]
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}E_{T}^{miss}$ [GeV$^{-1}$]
LogY=1
# END PLOT
