#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/JetShape.hh"

// For smearing
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/SmearedParticles.hh"
#include "Rivet/Projections/SmearedMET.hh"

#include <cassert>

namespace Rivet {


  /// FTAG analysis
  /// Author: Spyros Argyropoulos <spyridon.argyropoulos@cern.ch>
  
  class FTAG : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(FTAG);


    /// Book cuts and projections
    void init() {
    
      if ( getOption("SMEAR") == "ON" ) m_smear = true; // default is false - to turn ann use 
    
      // Cuts
      Cut eta_full       = (Cuts::abseta < 5.0);
      Cut lep_cuts       = (Cuts::abseta < 2.5) && (Cuts::pT > 7*GeV);

      // All final state particles
      FinalState fs(eta_full);

      // Get photons to dress leptons
      IdentifiedFinalState all_photons(fs);
      all_photons.acceptIdPair(PID::PHOTON);

      PromptFinalState photons(Cuts::abspid == PID::PHOTON, true);
      declare(photons, "photons");

      // Projection to find the electrons
      PromptFinalState electrons(Cuts::abspid == PID::ELECTRON, true);

      DressedLeptons dressedelectrons(photons, electrons, 0.1, lep_cuts);
      declare(dressedelectrons, "elecs");

      DressedLeptons ewdressedelectrons(all_photons, electrons, 0.1, eta_full);

      // Projection to find the muons
      PromptFinalState muons(Cuts::abspid == PID::MUON, true);

      DressedLeptons dressedmuons(photons, muons, 0.1, lep_cuts);
      declare(dressedmuons, "muons");

      DressedLeptons ewdressedmuons(all_photons, muons, 0.1, eta_full);

      // Projection to find MET
      declare(MissingMomentum(fs), "MET");
           
      // Jet clustering.
      VetoedFinalState vfs(fs);
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(jets, "jets");
            
      // --- Smeared objects --- //
      
      // Smeared Jets
      declare(SmearedJets(jets, JET_SMEAR_ATLAS_RUN2, JET_BTAG_ATLAS_RUN2_MV2C10), "s_jets");

      // Projection for smeared MET
      declare(SmearedMET(MissingMomentum(fs), MET_SMEAR_ATLAS_RUN2), "s_MET");
      
      // Electrons and muons
      // Note that ideally one would want to veto the smeared leptons from the smeared jets final state
      // but the difference should not be huge
      declare(SmearedParticles(dressedelectrons, ELECTRON_RECOEFF_ATLAS_RUN2, ELECTRON_SMEAR_ATLAS_RUN2), "s_elecs");
      declare(SmearedParticles(dressedmuons, MUON_EFF_ATLAS_RUN2, MUON_SMEAR_ATLAS_RUN2), "s_muons");
      
      // --- Histograms --- //
      
      // Inclusive histograms
      book(_h["allEvents"],    "allEvents",    1, 0., 2.);
      
      // Histograms after event selection
      book(_h["njets_sel"],        "njets_sel",         10, -0.5, 9.5);
      book(_h["nbjets_sel"],       "nbjets_sel",        6,  -0.5, 5.5);
      book(_h["Zprime_decay"],     "Zprime_decay",      17, -1,   16);
      book(_h["pT_Zprime"],        "pT_Zprime",         250, 0,   5000);
      book(_h["m_Zprime"],         "m_Zprime",          130, 0,   13000);
      book(_h["m_b1b2"],           "m_b1b2",            250, 0,   5000);
      book(_h["pT_b1"],            "pT_b1",             200, 0,   4000);
      book(_h["pT_b2"],            "pT_b2",             150, 0,   3000);
      book(_h["pT_c1"],            "pT_c1",             200, 0,   4000);
      book(_h["pT_c2"],            "pT_c2",             150, 0,   3000);
      book(_h["pT_l1"],            "pT_l1",             200, 0,   4000);
      book(_h["pT_l2"],            "pT_l2",             150, 0,   3000);
      book(_h["dR_b1b2"],          "dR_b1b2",           50,  0,   5);
      // Mathching Z decay to jets
      book(_h["dR_Z_b1"],          "dR_Z_b1",           50,  0,   5);
      book(_h["dR_Z_b2"],          "dR_Z_b2",           50,  0,   5);
      book(_h["dR_Z_c1"],          "dR_Z_c1",           50,  0,   5);
      book(_h["dR_Z_c2"],          "dR_Z_c2",           50,  0,   5);
      book(_h["dR_Z_l1"],          "dR_Z_l1",           50,  0,   5);
      book(_h["dR_Z_l2"],          "dR_Z_l2",           50,  0,   5);
      // b-jet plots
      book(_h["numB_b1"],          "numB_b1",           5,   0,   5);
      book(_h["numB_b2"],          "numB_b2",           5,   0,   5);
      book(_h["numC_b1"],          "numC_b1",           5,   0,   5);
      book(_h["numC_b2"],          "numC_b2",           5,   0,   5);
      book(_h["num_chg_B_b1"],     "num_chg_B_b1",	5,   0,   5);
      book(_h["num_chg_B_b2"],     "num_chg_B_b2",	5,   0,   5);
      book(_h["num_chg_C_b1"],     "num_chg_C_b1",	5,   0,   5);
      book(_h["num_chg_C_b2"],     "num_chg_C_b2",	5,   0,   5);
      book(_h["mindR_Bhad_b1"],    "mindR_Bhad_b1",     100, 0,   1);
      book(_h["maxdR_Bhad_b1"],    "maxdR_Bhad_b1",     100, 0,   1);
      book(_h["mindR_Bhad_b2"],    "mindR_Bhad_b2",     100, 0,   1);
      book(_h["maxdR_Bhad_b2"],    "maxdR_Bhad_b2",     100, 0,   1);
      book(_h["mindR_Chad_b1"],    "mindR_Chad_b1",     100, 0,   1);
      book(_h["maxdR_Chad_b1"],    "maxdR_Chad_b1",     100, 0,   1);
      book(_h["mindR_Chad_b2"],    "mindR_Chad_b2",     100, 0,   1);
      book(_h["maxdR_Chad_b2"],    "maxdR_Chad_b2",     100, 0,   1);
      book(_h["mindR_Bhad_ch_b1"], "mindR_Bhad_ch_b1",  100, 0,   1);
      book(_h["maxdR_Bhad_ch_b1"], "maxdR_Bhad_ch_b1",  100, 0,   1);
      book(_h["mindR_Bhad_ch_b2"], "mindR_Bhad_ch_b2",  100, 0,   1);
      book(_h["maxdR_Bhad_ch_b2"], "maxdR_Bhad_ch_b2",  100, 0,   1);
      book(_h["mindR_Chad_ch_b1"], "mindR_Chad_ch_b1",  100, 0,   1);
      book(_h["maxdR_Chad_ch_b1"], "maxdR_Chad_ch_b1",  100, 0,   1);
      book(_h["mindR_Chad_ch_b2"], "mindR_Chad_ch_b2",  100, 0,   1);
      book(_h["maxdR_Chad_ch_b2"], "maxdR_Chad_ch_b2",  100, 0,   1);
      // c-jet plots
      book(_h["numB_c1"],          "numB_c1",           5,   0,   5);
      book(_h["numB_c2"],          "numB_c2",           5,   0,   5);
      book(_h["numC_c1"],          "numC_c1",           5,   0,   5);
      book(_h["numC_c2"],          "numC_c2",           5,   0,   5);
      book(_h["num_chg_B_c1"],     "num_chg_B_c1",	5,   0,   5);
      book(_h["num_chg_B_c2"],     "num_chg_B_c2",	5,   0,   5);
      book(_h["num_chg_C_c1"],     "num_chg_C_c1",	5,   0,   5);
      book(_h["num_chg_C_c2"],     "num_chg_C_c2",	5,   0,   5);
      book(_h["mindR_Bhad_c1"],    "mindR_Bhad_c1",     100, 0,   1);
      book(_h["maxdR_Bhad_c1"],    "maxdR_Bhad_c1",     100, 0,   1);
      book(_h["mindR_Bhad_c2"],    "mindR_Bhad_c2",     100, 0,   1);
      book(_h["maxdR_Bhad_c2"],    "maxdR_Bhad_c2",     100, 0,   1);
      book(_h["mindR_Chad_c1"],    "mindR_Chad_c1",     100, 0,   1);
      book(_h["maxdR_Chad_c1"],    "maxdR_Chad_c1",     100, 0,   1);
      book(_h["mindR_Chad_c2"],    "mindR_Chad_c2",     100, 0,   1);
      book(_h["maxdR_Chad_c2"],    "maxdR_Chad_c2",     100, 0,   1);
      book(_h["mindR_Bhad_ch_c1"], "mindR_Bhad_ch_c1",  100, 0,   1);
      book(_h["maxdR_Bhad_ch_c1"], "maxdR_Bhad_ch_c1",  100, 0,   1);
      book(_h["mindR_Bhad_ch_c2"], "mindR_Bhad_ch_c2",  100, 0,   1);
      book(_h["maxdR_Bhad_ch_c2"], "maxdR_Bhad_ch_c2",  100, 0,   1);
      book(_h["mindR_Chad_ch_c1"], "mindR_Chad_ch_c1",  100, 0,   1);
      book(_h["maxdR_Chad_ch_c1"], "maxdR_Chad_ch_c1",  100, 0,   1);
      book(_h["mindR_Chad_ch_c2"], "mindR_Chad_ch_c2",  100, 0,   1);
      book(_h["maxdR_Chad_ch_c2"], "maxdR_Chad_ch_c2",  100, 0,   1);
      // Charged jet shapes
      book(_h["num_chg_b1"],       "num_chg_b1",	50,  0,   50);
      book(_h["num_chg_c1"],       "num_chg_c1",	50,  0,   50);
      book(_h["num_chg_l1"],       "num_chg_l1",	50,  0,   50);
      book(_h["dR_chg_b1"],        "dR_chg_b1",	        50,  0,   0.5);  
      book(_h["dR_chg_c1"],        "dR_chg_c1",	        50,  0,   0.5);     
      book(_h["dR_chg_l1"],        "dR_chg_l1",	        50,  0,   0.5);         
      book(_h["S_b1"],             "S_b1",	        50,  0,   0.5);
      book(_h["S_c1"],             "S_c1",	        50,  0,   0.5);
      book(_h["S_l1"],             "S_l1",	        50,  0,   0.5);
    }


    void analyze(const Event& event) {

      // Get the selected objects, using the projections.
      const Particles& electrons = (!m_smear) ? apply<DressedLeptons>(event, "elecs").particlesByPt() :
      					        apply<ParticleFinder>(event, "s_elecs").particlesByPt();
      const Particles& muons     = (!m_smear) ? apply<DressedLeptons>(event, "muons").particlesByPt() :
      						apply<ParticleFinder>(event, "s_muons").particlesByPt();
      const Jets& jets = (!m_smear) ? apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5) :
      				      apply<JetAlg>(event, "s_jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5);
      const Vector3 met = (!m_smear) ? apply<MissingMomentum>(event, "MET").vectorMPT() :
      				       apply<SmearedMET>(event, "s_MET").vectorMPT();

      Jets bjets, cjets, ljets;
      for (Jet jet : jets) {
        bool b_tagged = jet.bTagged(Cuts::pT > 5*GeV);
	bool c_tagged = jet.cTagged(Cuts::pT > 5*GeV);
        if      ( b_tagged              ) bjets += jet;
	else if ( c_tagged  & !b_tagged ) cjets += jet;
        else if ( !c_tagged & !b_tagged ) ljets += jet;
      }
      
      int njets  = jets.size();
      int nbjets = bjets.size();
      
      // Fill inclusive histograms
      _h["allEvents"]->fill(1.);
       
      // Pre-selection
      if ( njets < 2 ) vetoEvent;
      
      // Find Z' candidate
      Particle Zprime;
      findLastParticleById(Zprime, event, 32);
      
      // Find daughters of Z' candidate
      Particles firstQuarkDaughtersFromZprime;
      findFirstParticlesFromDecay(Zprime, firstQuarkDaughtersFromZprime);
      
      // Assert that the Z' decayed to 2 daughters
      assert(firstQuarkDaughtersFromZprime.size() == 2 && "\n\nERROR: The Z' has not decayed to 2 daughters!\n\n"); 
      
      // Find id of daughters and assert that their abs id is equal
      int id_daughter1 = firstQuarkDaughtersFromZprime.at(0).pid();
      int id_daughter2 = firstQuarkDaughtersFromZprime.at(1).pid();
      assert(abs(id_daughter1) == abs(id_daughter2) && "\n\nERROR: The Z' has decayed to daughters of different flavour!\n\n");
           
      // Determine Z' decay mode 
      _Zdecay = Zdecay::NONE;      
      if      (abs(id_daughter1) == 5 ) _Zdecay = Zdecay::BB;
      else if (abs(id_daughter1) == 4 ) _Zdecay = Zdecay::CC;
      else if (abs(id_daughter1) == 11) _Zdecay = Zdecay::EE;
      else if (abs(id_daughter1) == 15) _Zdecay = Zdecay::TT;
      else _Zdecay = Zdecay::LL;
           
      // Find jets from Z decay
      Jets bJets_Zdecay, cJets_Zdecay, lJets_Zdecay;
      if (_Zdecay == Zdecay::BB) {
        findJetsZdecay(bJets_Zdecay, bjets, firstQuarkDaughtersFromZprime);
      } 
      else if (_Zdecay == Zdecay::CC) {
        findJetsZdecay(cJets_Zdecay, cjets, firstQuarkDaughtersFromZprime);
      } 
      else if (_Zdecay == Zdecay::LL) {
        findJetsZdecay(lJets_Zdecay, ljets, firstQuarkDaughtersFromZprime);
      }  
      
      // Fill histograms after selection
      _h["njets_sel"]->fill(njets);
      _h["nbjets_sel"]->fill(nbjets);
      _h["Zprime_decay"]->fill(static_cast<int>(_Zdecay));
      _h["pT_Zprime"]->fill(Zprime.pT()*GeV);
      _h["m_Zprime"]->fill(Zprime.mass()*GeV);
      
      if (bJets_Zdecay.size() > 0) _h["pT_b1"]->fill(bJets_Zdecay.at(0).pT()*GeV);
      if (bJets_Zdecay.size() > 1) {
        _h["pT_b2"]->fill(bJets_Zdecay.at(1).pT()*GeV);
	_h["dR_b1b2"]->fill(deltaR(bJets_Zdecay.at(0).momentum(), bJets_Zdecay.at(1).momentum()));
      }
      
      // bb decays
      if (_Zdecay == Zdecay::BB) {
	if (bJets_Zdecay.size() > 0) { 
	  fillJetPlots(bJets_Zdecay.at(0), 0); 
	  fillJetShape(bJets_Zdecay.at(0), 0); 
	  float dR1 = deltaR(firstQuarkDaughtersFromZprime.at(0).momentum(), bJets_Zdecay.at(0).momentum());
	  float dR2 = deltaR(firstQuarkDaughtersFromZprime.at(1).momentum(), bJets_Zdecay.at(0).momentum());
	  _h["dR_Z_b1"]->fill(std::min(dR1, dR2));
	}
	if (bJets_Zdecay.size() > 1) {
	  fillJetPlots(bJets_Zdecay.at(1), 1);	 
	  float dR1 = deltaR(firstQuarkDaughtersFromZprime.at(0).momentum(), bJets_Zdecay.at(1).momentum());
	  float dR2 = deltaR(firstQuarkDaughtersFromZprime.at(1).momentum(), bJets_Zdecay.at(1).momentum());
	  _h["dR_Z_b2"]->fill(std::min(dR1, dR2));
	}        
      }
      // cc decays
      else if (_Zdecay == Zdecay::CC) {
	if (cJets_Zdecay.size() > 0) { 
	  fillJetPlots(cJets_Zdecay.at(0), 0); 
	  fillJetShape(cJets_Zdecay.at(0), 0); 
	  float dR1 = deltaR(firstQuarkDaughtersFromZprime.at(0).momentum(), cJets_Zdecay.at(0).momentum());
	  float dR2 = deltaR(firstQuarkDaughtersFromZprime.at(1).momentum(), cJets_Zdecay.at(0).momentum());
	  _h["dR_Z_c1"]->fill(std::min(dR1, dR2));
	} 
	if (cJets_Zdecay.size() > 1) {
	  fillJetPlots(cJets_Zdecay.at(1), 1);
	  float dR1 = deltaR(firstQuarkDaughtersFromZprime.at(0).momentum(), cJets_Zdecay.at(1).momentum());
	  float dR2 = deltaR(firstQuarkDaughtersFromZprime.at(1).momentum(), cJets_Zdecay.at(1).momentum());	
	  _h["dR_Z_c2"]->fill(std::min(dR1, dR2));
	}      
      }
      // ll decays
      else if (_Zdecay == Zdecay::LL) {
	if (lJets_Zdecay.size() > 0) { 
	  fillJetPlots(lJets_Zdecay.at(0), 0); 
	  fillJetShape(lJets_Zdecay.at(0), 0); 
	  float dR1 = deltaR(firstQuarkDaughtersFromZprime.at(0).momentum(), lJets_Zdecay.at(0).momentum());
	  float dR2 = deltaR(firstQuarkDaughtersFromZprime.at(1).momentum(), lJets_Zdecay.at(0).momentum());
	  _h["dR_Z_l1"]->fill(std::min(dR1, dR2));
	} 
	if (lJets_Zdecay.size() > 1) {
	  fillJetPlots(lJets_Zdecay.at(1), 1);	 
	  float dR1 = deltaR(firstQuarkDaughtersFromZprime.at(0).momentum(), lJets_Zdecay.at(1).momentum());
	  float dR2 = deltaR(firstQuarkDaughtersFromZprime.at(1).momentum(), lJets_Zdecay.at(1).momentum());
	  _h["dR_Z_l2"]->fill(std::min(dR1, dR2));
	}        
      }
      
    } // end of analyze function
     
    
    
    // Histogram filling function
    void fillJetPlots(const Jet &jet, const int jetPos) {
 
      Particles btags = jet.bTags();
      Particles ctags = jet.cTags();	  
      vector<float> dR_values_B, dR_values_C;
      vector<float> dR_values_B_ch, dR_values_C_ch;
      int num_charged_B(0), num_charged_C(0);
      for (const auto p : btags) {
        float dR = deltaR(p.momentum(), jet.momentum());
        dR_values_B.push_back(dR);
        if (p.isCharged()) { 
          ++num_charged_B;
          dR_values_B_ch.push_back(dR);
        }
      }
      for (const auto p : ctags) {
        float dR = deltaR(p.momentum(), jet.momentum());
        dR_values_C.push_back(dR);
        if (p.isCharged()) {
          ++num_charged_C;
          dR_values_C_ch.push_back(dR);
        }
      }
      
      if (_Zdecay == Zdecay::BB) {
        if (jetPos == 0) {
          _h["numB_b1"]->fill(btags.size());
          _h["numC_b1"]->fill(ctags.size());
          _h["num_chg_B_b1"]->fill(num_charged_B);
          _h["num_chg_C_b1"]->fill(num_charged_C);
          if (dR_values_B.size() > 0) {
            _h["mindR_Bhad_b1"]->fill(*std::min_element(dR_values_B.begin(), dR_values_B.end()));
            _h["maxdR_Bhad_b1"]->fill(*std::max_element(dR_values_B.begin(), dR_values_B.end()));
          }
          if (dR_values_C.size() > 0) {
            _h["mindR_Chad_b1"]->fill(*std::min_element(dR_values_C.begin(), dR_values_C.end()));
            _h["maxdR_Chad_b1"]->fill(*std::max_element(dR_values_C.begin(), dR_values_C.end()));
          }
          if (dR_values_B_ch.size() > 0) {
            _h["mindR_Bhad_ch_b1"]->fill(*std::min_element(dR_values_B_ch.begin(), dR_values_B_ch.end()));
            _h["maxdR_Bhad_ch_b1"]->fill(*std::max_element(dR_values_B_ch.begin(), dR_values_B_ch.end()));
          }
          if (dR_values_C_ch.size() > 0) {
          _h["mindR_Chad_ch_b1"]->fill(*std::min_element(dR_values_C_ch.begin(), dR_values_C_ch.end()));
              _h["maxdR_Chad_ch_b1"]->fill(*std::max_element(dR_values_C_ch.begin(), dR_values_C_ch.end()));
          }
        } 
        else if (jetPos == 1) {
          _h["numB_b2"]->fill(btags.size());
          _h["numC_b2"]->fill(ctags.size());
          _h["num_chg_B_b2"]->fill(num_charged_B);
          _h["num_chg_C_b2"]->fill(num_charged_C);
          if (dR_values_B.size() > 0) {
            _h["mindR_Bhad_b2"]->fill(*std::min_element(dR_values_B.begin(), dR_values_B.end()));
            _h["maxdR_Bhad_b2"]->fill(*std::max_element(dR_values_B.begin(), dR_values_B.end()));
          }
          if (dR_values_C.size() > 0) {
            _h["mindR_Chad_b2"]->fill(*std::min_element(dR_values_C.begin(), dR_values_C.end()));
            _h["maxdR_Chad_b2"]->fill(*std::max_element(dR_values_C.begin(), dR_values_C.end()));
          }
          if (dR_values_B_ch.size() > 0) {
            _h["mindR_Bhad_ch_b2"]->fill(*std::min_element(dR_values_B_ch.begin(), dR_values_B_ch.end()));
            _h["maxdR_Bhad_ch_b2"]->fill(*std::max_element(dR_values_B_ch.begin(), dR_values_B_ch.end()));
          }
          if (dR_values_C_ch.size() > 0) {
            _h["mindR_Chad_ch_b2"]->fill(*std::min_element(dR_values_C_ch.begin(), dR_values_C_ch.end()));
            _h["maxdR_Chad_ch_b2"]->fill(*std::max_element(dR_values_C_ch.begin(), dR_values_C_ch.end()));
          }
        } 
      } else if (_Zdecay == Zdecay::CC) {
        if (jetPos == 0) {
          _h["numB_c1"]->fill(btags.size());
          _h["numC_c1"]->fill(ctags.size());
          _h["num_chg_B_c1"]->fill(num_charged_B);
          _h["num_chg_C_c1"]->fill(num_charged_C);
          if (dR_values_B.size() > 0) {
            _h["mindR_Bhad_c1"]->fill(*std::min_element(dR_values_B.begin(), dR_values_B.end()));
            _h["maxdR_Bhad_c1"]->fill(*std::max_element(dR_values_B.begin(), dR_values_B.end()));
          }
          if (dR_values_C.size() > 0) {
            _h["mindR_Chad_c1"]->fill(*std::min_element(dR_values_C.begin(), dR_values_C.end()));
            _h["maxdR_Chad_c1"]->fill(*std::max_element(dR_values_C.begin(), dR_values_C.end()));
          }
          if (dR_values_B_ch.size() > 0) {
            _h["mindR_Bhad_ch_c1"]->fill(*std::min_element(dR_values_B_ch.begin(), dR_values_B_ch.end()));
            _h["maxdR_Bhad_ch_c1"]->fill(*std::max_element(dR_values_B_ch.begin(), dR_values_B_ch.end()));
          }
          if (dR_values_C_ch.size() > 0) {
          _h["mindR_Chad_ch_c1"]->fill(*std::min_element(dR_values_C_ch.begin(), dR_values_C_ch.end()));
              _h["maxdR_Chad_ch_c1"]->fill(*std::max_element(dR_values_C_ch.begin(), dR_values_C_ch.end()));
          }
        } 
        else if (jetPos == 1) {
          _h["numB_c2"]->fill(btags.size());
          _h["numC_c2"]->fill(ctags.size());
          _h["num_chg_B_c2"]->fill(num_charged_B);
          _h["num_chg_C_c2"]->fill(num_charged_C);
          if (dR_values_B.size() > 0) {
            _h["mindR_Bhad_c2"]->fill(*std::min_element(dR_values_B.begin(), dR_values_B.end()));
            _h["maxdR_Bhad_c2"]->fill(*std::max_element(dR_values_B.begin(), dR_values_B.end()));
          }
          if (dR_values_C.size() > 0) {
            _h["mindR_Chad_c2"]->fill(*std::min_element(dR_values_C.begin(), dR_values_C.end()));
            _h["maxdR_Chad_c2"]->fill(*std::max_element(dR_values_C.begin(), dR_values_C.end()));
          }
          if (dR_values_B_ch.size() > 0) {
            _h["mindR_Bhad_ch_c2"]->fill(*std::min_element(dR_values_B_ch.begin(), dR_values_B_ch.end()));
            _h["maxdR_Bhad_ch_c2"]->fill(*std::max_element(dR_values_B_ch.begin(), dR_values_B_ch.end()));
          }
          if (dR_values_C_ch.size() > 0) {
            _h["mindR_Chad_ch_c2"]->fill(*std::min_element(dR_values_C_ch.begin(), dR_values_C_ch.end()));
            _h["maxdR_Chad_ch_c2"]->fill(*std::max_element(dR_values_C_ch.begin(), dR_values_C_ch.end()));
          }
        } 
      }   
 
    } // end of fillBBplots


    // Function for filling jet shape histograms with charged constituents
    void fillJetShape(const Jet &jet, const int jetPos) {
      
      pair<int, vector<float>> Nch_dR;
      getCharged(jet, Nch_dR);
            
      if (_Zdecay == Zdecay::BB) {
        if (jetPos == 0) {
          _h["num_chg_b1"]->fill(Nch_dR.first);
          for (auto dR : Nch_dR.second) _h["dR_chg_b1"]->fill(dR);
          fillIntJetShapeHisto(_h["S_b1"], Nch_dR.second);
	}
      }	
      else if (_Zdecay == Zdecay::CC) {
        if (jetPos == 0) {
          _h["num_chg_c1"]->fill(Nch_dR.first);
          for (auto dR : Nch_dR.second) _h["dR_chg_c1"]->fill(dR);
          fillIntJetShapeHisto(_h["S_c1"], Nch_dR.second);
	}
      }	
      else if (_Zdecay == Zdecay::LL) {
        if (jetPos == 0) {
          _h["num_chg_l1"]->fill(Nch_dR.first);
          for (auto dR : Nch_dR.second) _h["dR_chg_l1"]->fill(dR);
          fillIntJetShapeHisto(_h["S_l1"], Nch_dR.second);
	}
      }
      
               
    } // end of fillJetShape
    
    
    // Fill histogram with integrated jet shape
    void fillIntJetShapeHisto(Histo1DPtr h, const vector<float> dR) {
      // Get bins
      vector<double> xEdges = h->xEdges();
      for (unsigned int i=0; i < xEdges.size()-1; ++i) {
        double highEdge = xEdges.at(i+1);
	double lowEdge  = xEdges.at(i);
	h->fill(lowEdge, NpartWithin(dR, highEdge));
      }
    }
    
    // Function to associate jets to Z' decay products
    void findJetsZdecay(Jets &jets_decay, const Jets &jets, const Particles &Zdaughters) {
      for (auto d : Zdaughters) {
        float mindR = 999.;
	Jet matchedJet;
        for (auto jet : jets) {
          float dR = deltaR(jet.momentum(), d.momentum());
	  if (dR < mindR) {
	    matchedJet = jet;
	    mindR = dR;
	  }  
        }
	jets_decay.push_back(matchedJet);
      }
    }
    
    // Normalise histograms
    void finalize() {
      /*
      // Normalize to cross-section
      const double sf = (crossSection() / sumOfWeights());
      for (auto hist : _h) {
        scale(hist.second, sf);
        // Normalized distributions
        if (hist.first.find("_norm") != string::npos)  normalize(hist.second);
      }
      */
    }


    /* -------- Truth particle helper functions -------- */
    
    // For a given PDG ID locate its last occurence in the truth record
    // Last occurence = the particle decays to particles with different PDG ID
    void findLastParticleById(Particle &part, const Event & event, const int id) {
      const GenEvent *genEvent = event.genEvent();
      const vector<ConstGenParticlePtr> genParticles = HepMCUtils::particles(genEvent);
      for (auto p : genParticles) {
        if ( p->pdg_id() == id) {
	  bool isLast = true;
          ConstGenVertexPtr dv = p->end_vertex();
          if (dv != nullptr) {
            // Loop over all daughter particles and check if they contain a particle with the same pdg id
            for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
              if (abs(pp->pdg_id()) == id) isLast = false;
            }
	  }
	  if (isLast) part = Particle(p);
	}
      }
    }
    
    // Get daughter particles from mother particle
    void findFirstParticlesFromDecay(const Particle &mother, Particles &daughters) {
      ConstGenVertexPtr dv = mother.genParticle()->end_vertex();
      for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
        daughters.push_back(Particle(pp));
      }
    }
    
    // Check if the particle is the last particle in the chain
    bool isLastInChain(ConstGenParticlePtr p) {
      bool isLast = true;
      ConstGenVertexPtr dv = p->end_vertex();
      for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
        if ( pp->pdg_id() == p->pdg_id() ) { isLast = false; break; }
      }
      return isLast;
    }
    
    // Helper function for charged jet shapes
    void getCharged(const Jet& jet, pair<int, vector<float>> &Nch_dR) {
      Particles constituents = jet.constituents();
      int ncharged = 0;
      vector<float> dR_charged;
      for (const auto p : constituents) {
        if (p.isCharged()) { 
	  ++ncharged;
	  dR_charged.push_back(deltaR(p.momentum(), jet.momentum()));
	}  
      }
      Nch_dR = {ncharged, dR_charged};
    }
    
    // Function that returns number of constituents within a given radius R
    int NpartWithin(const vector<float> &dR, const float R) {
      int N=0;
      for (auto i : dR) if (i <= R) ++N;
      return N;
    }
    
    /* ------------------------------------------------- */

  private:

    /// @name Objects that are used by the event selection decisions
    map<string, Histo1DPtr> _h;
    
    bool m_smear = false;
    
    // Zdecay mode
    enum class Zdecay { BB=5, CC=4, LL=1, TT=15, EE=11, NONE=-1 }; 
    
    Zdecay _Zdecay = Zdecay::NONE;

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(FTAG);


}
