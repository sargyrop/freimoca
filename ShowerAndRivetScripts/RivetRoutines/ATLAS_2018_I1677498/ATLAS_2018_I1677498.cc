#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// WWbb at 13 TeV
  class ATLAS_2018_I1677498 : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(ATLAS_2018_I1677498);


    /// Book cuts and projections
    void init() {

      if ( getOption("VETOTOPISR") == "PARENT" ) _veto_top = true;
      if ( getOption("VETOTOPISR") == "GRANDPARENT" ) { _veto_top = true; _veto_top_gp = true; }

      // All final state particles
      FinalState fs(Cuts::abseta < 5.0);

      PromptFinalState photons(Cuts::abspid == PID::PHOTON, true); // true accepts tau decays

      PromptFinalState bare_el(Cuts::abspid == PID::ELECTRON, true); // true accepts tau decays
      DressedLeptons elecs(photons, bare_el, 0.1, Cuts::pT > 7*GeV && Cuts::abseta < 2.47);
      declare(elecs, "elecs");

      PromptFinalState bare_mu(Cuts::abspid == PID::MUON, true); // accepts tau decays
      DressedLeptons muons(photons, bare_mu, 0.1, Cuts::pT > 6*GeV && Cuts::abseta < 2.5);
      declare(muons, "muons");

      FastJets jets(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(jets, "jets");

      book(_h, 3, 1, 1);
      book(_h_gluons, "h_gluons", 3, 0, 3);
    }


    void analyze(const Event& event) {

      // Identify bjets (signal), light jets (OR) and soft b-jets (veto)
      size_t soft_bjets = 0;
      Jets bjets, lightjets;
      for (const Jet& jet : apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 5*GeV)) {
        bool isBjet = jet.bTagged(Cuts::pT > 5*GeV);
        if (isBjet) soft_bjets += 1;
        if (jet.abseta() < 2.5) {
          if ( isBjet && jet.pT() > 25*GeV) bjets += jet;
          if (!isBjet && jet.pT() > 20*GeV) lightjets += jet;
        }
      }
      if (soft_bjets != 2) vetoEvent;
      if (bjets.size() != 2) vetoEvent;

      // Get dressed leptons
      vector<DressedLepton> leptons;
      for (auto& lep : apply<DressedLeptons>(event, "muons").dressedLeptons()) { leptons.push_back(lep); }
      for (auto& lep : apply<DressedLeptons>(event, "elecs").dressedLeptons()) { leptons.push_back(lep); }

      // 1. Find which light jets survive OR
      for (const auto& lep : leptons) {
        ifilter_discard(lightjets, [&](const Jet& jet) {
          return deltaR(jet, lep) < 0.2 && (lep.abspid() == PID::ELECTRON || lep.pT()/jet.pT() > 0.7);
        });
      }

      // 2. Find which leptons survive the OR and apply signal selection
      for (const auto& jet : (lightjets + bjets)) {
        ifilter_discard(leptons, [&](const DressedLepton& lep) {
          return lep.pT() < 28*GeV || deltaR(jet, lep) < min(0.4, 0.04+10*GeV/lep.pT());
        });
      }

      if (leptons.size() != 2) vetoEvent;
      std::sort(leptons.begin(), leptons.end(), cmpMomByPt);

      // Z veto
      const size_t nEl = count(leptons, [](const DressedLepton& lep) { return  lep.abspid() == PID::ELECTRON; });
      const double mll = (leptons[0].mom() + leptons[1].mom()).mass();
      if (nEl != 1 && !(fabs(mll - 91*GeV) > 15*GeV && mll > 10*GeV)) vetoEvent;
      
      // Top ISR veto
      if (_veto_top) {
        if (n_gluons_from_top(event) > 0) vetoEvent; 
	else _h_gluons->fill(0); 
      }
      
      const double m00 = (leptons[0].mom() + bjets[0].mom()).mass();
      const double m10 = (leptons[1].mom() + bjets[0].mom()).mass();
      const double m01 = (leptons[0].mom() + bjets[1].mom()).mass();
      const double m11 = (leptons[1].mom() + bjets[1].mom()).mass();
      const double minimax = min( max(m00,m11), max(m10,m01) );
      _h->fill(minimax/GeV);
    }


    /// Finalise
    void finalize() {
      normalize(_h);
    }
  
  protected:
    
    // Function to count gluons emitted from top quarks
    int n_gluons_from_top(const Event & event){
      const GenEvent *genEvent = event.genEvent();
      const auto genParticles = HepMCUtils::particles(genEvent);
      int n_vetoed_gluons = 0;
      int n_vetoed_gluons_fromgp = 0;

      // loop over all particles parents
      for (auto p : genParticles){
        if( p->pdg_id() == 21 ){
          ConstGenVertexPtr pv = p->production_vertex();   // particles in the p vertex
          if (pv!=nullptr){
            for(ConstGenParticlePtr pp: HepMCUtils::particles(pv, Relatives::PARENTS)){                        
	      if (_veto_top_gp) {
	        n_vetoed_gluons += count_grandparents(p);
	      }
              if( abs(pp->pdg_id()) == 6 ) {	// && abs(pp->status()) == 23	?   
                ++n_vetoed_gluons;
              }
            } 
          } 
        }
      }   
      if (n_vetoed_gluons_fromgp == 0 && n_vetoed_gluons > 0) _h_gluons->fill(1);
      return n_vetoed_gluons;
    };
    
    int count_grandparents(ConstGenParticlePtr particle){
      int n_vetoed_gluons = 0;
      ConstGenVertexPtr pv = particle->production_vertex();
      if (pv!=nullptr){
        for(ConstGenParticlePtr pp: HepMCUtils::particles(pv, Relatives::PARENTS)){			   
          if( abs(pp->pdg_id()) == 6 ) {	    // && abs(pp->status()) == 23 ?   
            ++n_vetoed_gluons;
          }
        } 
      }    
      if (n_vetoed_gluons) _h_gluons->fill(2);
      return n_vetoed_gluons;   
    }
    

  private:

    /// Histogram
    Histo1DPtr _h, _h_gluons;
    
    /// Veto emissions off top
    bool _veto_top = false;
    bool _veto_top_gp = false;

  };


  DECLARE_RIVET_PLUGIN(ATLAS_2018_I1677498);

}
