#include "Rivet/Analysis.hh"
#include "Rivet/Projections/PromptFinalState.hh"

namespace Rivet {
  
  class MC_Z : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_Z);


    /// Book cuts and projections
    void init() {
        
      // Declare cuts used for particle selection
      Cut lep_cuts = (Cuts::abseta < 2.5) & (Cuts::pT > 20*GeV);

      // Projection to find the electrons
      PromptFinalState electrons((Cuts::abspid == PID::ELECTRON) & lep_cuts);
      declare(electrons, "Electrons");
      
      // --- Histograms --- //
      
      book(_h["m_ee"], "m_ee", 100, 0, 200);
      
    }


    void analyze(const Event& event) {

      // Get the selected objects, using the projections defined in init()
      const Particles& electrons = apply<PromptFinalState>(event, "Electrons").particlesByPt();

      // Apply the event selection
      
      // Check that the event has only 2 leptons of opposite charge
      //bool has2leptons = ...
      
      // Reconstruct the invariant mass of the 
      // float mee = ...
      
      // Find Z boson 
      // Particle Zboson.
      //findLastParticleById(...);
      
      // Fill histograms
      //_h["m_ee"]->fill(mee);
      
    }


    void finalize() {
      /*
      // Normalize to cross-section
      const double sf = (crossSection() / sumOfWeights());
      for (auto hist : _h) {
        scale(hist.second, sf);
        // Normalized distributions
        if (hist.first.find("_norm") != string::npos)  normalize(hist.second);
      }
      */
    }


  private:
    
    // Function to find last particle by id
    void findLastParticleById(Particle &part, const Event & event, const int id) {
      const GenEvent *genEvent = event.genEvent();
      const vector<ConstGenParticlePtr> genParticles = HepMCUtils::particles(genEvent);
      for (auto p : genParticles) {
        if ( p->pdg_id() == id) {
	  bool isLast = true;
          ConstGenVertexPtr dv = p->end_vertex();
          if (dv != nullptr) {
            // Loop over all daughter particles and check if they contain a particle with the same pdg id
            for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
              if (abs(pp->pdg_id()) == id) isLast = false;
            }
	  }
	  if (isLast) part = Particle(p);
	}
      }
    }
    
    /// @name Objects that are used by the event selection decisions
    map<string, Histo1DPtr> _h;

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_Z);

}
