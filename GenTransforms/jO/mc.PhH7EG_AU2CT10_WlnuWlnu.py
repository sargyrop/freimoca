#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Powheg+Herwig7 WW->lvlv with H7UE tune'
evgenConfig.keywords = [ "SM", "WW" ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.contact = [ 'spyridon.argyropoulos@cern.ch' ]
# 11k events per file
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1
evgenConfig.tune = "H7.1-Default"
