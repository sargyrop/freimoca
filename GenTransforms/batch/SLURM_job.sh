#!/bin/bash
#SBATCH -p express

#Powhegname=$1 # Filename Powhegname
#Powhegfile=$2 # Absolute Path and Filename /path/to/file/Powhegname

#Powhegname=$(echo $Powhegfile | awk -F '/' '{print $NF}')

FILES=$1
Inputdirectory=$2
Outputdirectory=$3

# convert comma seperated filelist FILES to array variable
Filearray=($(echo $FILES | awk -F ',' '{for (i=1; i<NF; i++) printf $i " "; if (NF >=1) print $NF; }'))

###########################
# Setting directory paths #
###########################

CURDIR=$(pwd)

# Check and or create TMPDIR
if [[ -v TMPDIR ]] ; then
	echo "TMPDIR exists"
	echo "TMPDIR is:    " $TMPDIR
else
	echo "Setting TMPDIR"
	TMPDIR=$(mktemp -d)              # create temporary directory in /tmp and store name
	echo "TMPDIR is:    " $TMPDIR
fi

echo "temporary dir:" $TMPDIR            # directory where BFG job is executed
echo "working dir:  " $CURDIR            # current directory

######################
# Set up environment #
######################

# Copy runTransform to TMPDIR
cp ../scripts/runTransform.sh $TMPDIR

# Check if cp worked
if [ ! -f "$TMPDIR/runTransform.sh" ] ; then
	echo "ERROR while copying runTransform.sh"
	exit 1
fi

# Copy and Check all other Files
for File in ${Filearray[@]}
do
    cp $Inputdirectory/$File $TMPDIR
    if [ ! -f "$TMPDIR/$File" ] ; then
        echo "ERROR while copying $File"
        exit 2
    fi
done

#################################
# Run runTransform on Powhegfile#
#################################

# Needed for runTransform.sh
JOFILE=$CURDIR/../jO/mc.PhH7EG_AU2CT10_WlnuWlnu.py

#change path to TMPDIR
cd $TMPDIR

mkdir temp_output

echo "starting runTransform.sh"

# looping over Filearray
for File in ${Filearray[@]}
do
    echo "sending File $File to runTransform"
    #./runTransform.sh $JOFILE $Inputdirectory/$File AthGeneration,21.6.17 123456 $PWD/temp_output
    touch temp_output/$File
    echo "sent"
done

echo "copying output"

cp -R temp_output/* $Outputdirectory

printf \\n

# Deleting TMPDIR
echo "cleaning up"
rm -rf $TMPDIR

