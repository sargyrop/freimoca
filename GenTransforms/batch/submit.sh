#!/bin/bash

###################
# Global Settings #
###################

standardInputdirectory=/work/ws/atlas/sa1083-MCSamples/mc15_13TeV.361591.PhHppEG_AU2CT10_WlnuWlnu.evgen.TXT.e4696
standardOutputdirectory=/work/ws/atlas/mo113-PH7_EVNT

FilesPerBatch=${1:-"50"}
Inputdirectory=${2:-$standardInputdirectory}
Outputdirectory=${3:-$standardOutputdirectory}

# Check if arguments are provided or take standart values
if [ "$#" == 0 ]; then
    echo "Using standard Values:"
    echo "Files per Batch:" $FilesPerBatch
    echo "Inputdirectory: " $Inputdirectory
    echo "Outputdirectory:" $Outputdirectory
elif [ "$#" != 3 ]; then
    echo "Usage: ./submit.sh FilesPerBatch INDIR OUTDIR"
    exit 1
else
     # range for acceptable value
    if [ $FilesPerBatch -lt 1 ] || [ $FilesPerBatch -gt 80 ]; then
        echo "Not a valid Number! FilesPerBatch must be between 1 and 80"
        echo "Using standard Value of 50"
        FilesPerBatch=50
    fi
    if [ ! -d $Inputdirectory ]; then
        echo "Inputdirectory: $Inputdirectory does not exist"
        exit 2
    fi
    if [ ! -d $Outputdirectory ]; then
        echo "Outputdirectory: $Outputdirectory does not exist"
        exit 3
    fi
fi

#########################################
# putting Filenames into array variable #
#########################################

all_files=( $(ls $Inputdirectory/ | cat) )
done_files=( $(ls $Outputdirectory/ | cat) )

# aborting if no input files are found
[ ${#all_files[@]} -eq 0 ] && echo "No Input files found" && exit 4

###############################################
# looping over Files and adding missing files #
###############################################

declare -a missingfiles

counter=1 #Debug

#all_files=("${all_files[@]:0:4}") #Debug

echo "Selecting Missing Files:"

for file in "${all_files[@]}" # "" needed for whitespaces in names
do
    ##### #Debug
    # gives aproximate percentage reading
    if [[ $(( $counter%120 )) == 0 ]]; then
        echo -en "\rProgress: $(( $counter / 120))%"
    fi
    #####

    # get number tag and constructing outputfilename
    OutputFilename=$(echo $file | awk -F '.' '{printf "EVNT.%s%s.pool.root" , $2, $3}')

    # check if outputfile alrady exists and adding to missing if not
    if [[ -n $(find $Outputdirectory/ -name $OutputFilename) ]]; then
        echo "duplicate file"
    else
	missingfiles+=( "$file" )
    fi
    counter=$(( $counter + 1 )) #Debug
done

echo ""

echo "Number of missing Files:"

echo ${#missingfiles[@]}

echo "Number of completed Files:"

echo ${#done_files[@]}

###############################################
# Bunching Files into Packages to send as job #
###############################################

#calculate the number of the packets -1 (loop starts from 0)
packets=$(( ${#missingfiles[@]}/$FilesPerBatch))

echo "Number of packets: $(( $packets+1 ))"

# looping over packets and adding missingfile names to newpacket
for (( i = 0; i <= $packets; i++))
do
    begin=$(( 0 + $i * $FilesPerBatch ))

    # array containing Files for Package
    newpacket=("${missingfiles[@]:$begin:$FilesPerBatch}")

    # skips step if last packet ends up empty
    [ ${#newpacket[@]} == 0 ] && continue

    echo "Packet $(( $i+1 )) of length: ${#newpacket[@]}"

    # converting array to commaseperated list string
    for j in ${newpacket[@]}; do Filelist=$(echo $Filelist$j,);done

    Filelist=${Filelist::-1} # remove last comma

    ########################
    # Editing job_template #
    ########################

    # name for job and log file
    first=$(echo ${newpacket[0]} | awk -F '.' '{printf "%s%s" , $2, $3}')
    last=$(echo ${newpacket[-1]} | awk -F '.' '{printf "%s%s" , $2, $3}')
    name=$first-$last-${#newpacket[@]}

    # creating new jobtemplate
    sed "s|FILES|$Filelist|g" job_template > temp_job_template
    sed -i "s|INDIR|$Inputdirectory|g" temp_job_template
    sed -i "s|OUTDIR|$Outputdirectory|g" temp_job_template
    sed -i "s|JOBNAME|Job$name|g" temp_job_template
    sed -i "s|OUTPUTLOG|Log$name|g" temp_job_template

    echo "Job: $(( $i+1 )), Template:"
    cat -n temp_job_template

    # SEND JOBS HERE:

    sbatch temp_job_template

    rm -f temp_job_template
    
    # clearing Filelist for next package
    Filelist="" 
done

echo "All jobs sent"
